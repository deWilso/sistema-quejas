$("#formulario").validate({
    errorClass: "my-error-class",
    validClass: "my-valid-class",
    rules: {
        primer_nombre: {
            required: true,

        },
        action: "required",

        action: "required",
        primer_apellido: {
            required: true,

        },
        action: "required",
        cpu: {
            required: true,

        },
        action: "required",
        ram: {
            required: true,

        },
        action: "required",
        so: {
            selectcheck: true,

        },
        action: "required",
        hdd: {
            required: true,

        },
        action: "required",
    },
    messages: {
        primer_nombre: {
            required: "Campo requerido",
        },
        action: "por favor provea la informacion",
        primer_apellido: {
            required: "Campo requerido",
        },
        action: "por favor provea la informacion",
        serie: {
            required: "Campo requerido",
        },
        action: "por favor provea la informacion",
        cpu: {
            required: "Campo requerido",
        },
        action: "por favor provea la informacion",
        ram: {
            required: "Campo requerido",
        },
        action: "por favor provea la informacion",
        hdd: {
            required: "Campo requerido",
        },
        action: "por favor provea la informacion",
    }
});
jQuery.validator.addMethod('selectcheck', function(value) {
    return (value != '0');
}, "Campo requerido");