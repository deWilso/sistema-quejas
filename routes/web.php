<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

/*----------------------------------------------------------*/
//Quejas

Route::get('/', 'QuejaController@index')->name('quejas');
Route::post('/quejaStore', 'QuejaController@store')->name('storeQueja');
Route::get('/showQueja', 'QuejaController@showQueja')->name('showQueja');
Route::get('/getQueja', 'QuejaController@getQueja')->name('getQueja');
Route::get('/getMunicipios', 'QuejaController@getMunicipios')->name('getMunicipios');

Route::get('/lista_quejas', 'QuejaController@administrativeHome')->name('administrativeHome')->middleware('auth');
Route::get('/queja/{id?}/', 'QuejaController@edit')->name('actualizaQuejaForm');
Route::post('/queja/{id?}/', 'QuejaController@update')->name('actualizarQueja');

Route::get('/filtroRangoFechaView', 'QuejaController@filtroRangoFechaView')->name('filtroRangoFechaView');
Route::get('/quejaFiltrosFecha', 'QuejaController@quejaFiltrosFecha')->name('quejaFiltrosFecha');
Route::get('/filtroRegionView','QuejaController@filtroRegionView')->name('filtroRegionView');
Route::get('/quejaFiltrosRegion', 'QuejaController@quejaFiltrosRegion')->name('quejaFiltrosRegion');


Route::get('/lista_usuarios', 'UsersController@index')->name('lista_usuarios');
Route::get('/usuario/{id?}', 'UsersController@edit')->name('actualizaUsuarioForm');
Route::post('/usuario/{id?}', 'UsersController@update')->name('actualizarUsuario');
Route::post('usuario/{id?}/eliminar', 'UsersController@destroy')->name('eliminaUsuario');
Route::get('/createUsuario', 'UsersController@create')->name('createUsuario');
Route::post('/saveUser', 'UsersController@store')->name('saveUser');

Route::get('/filtroDepartamentoView', 'QuejaController@filtroDepartamentoView')->name('filtroDepartamentoView');
Route::get('/quejaFiltrosDepartamento', 'QuejaController@quejaFiltrosDepartamento')->name('quejaFiltrosDepartamento');


Route::get('/sixMonths', 'QuejaController@sixMonths')->name('sixMonths');
