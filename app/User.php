<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'primer_nombre', 'segundo_nombre','primer_apellido', 'segundo_apellido','apellido_casada','departamento','municipio', 'sede','direccion','identificacion',
        'telefono', 'fecha_nacimiento', 'genero','estado_civil','rol', 'password', 'correo_verificado_por', 'correo'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function depto_usuario(){
        return $this->belongsTo('App\Catalogo', 'departamento');
    }

    public function muni_usuario(){
        return $this->belongsTo('App\Catalogo', 'municipio');
    }
    public function sede_usuario(){
        return $this->belongsTo('App\Catalogo', 'sede');
    }
    public function rol_usuario(){
        return $this->belongsTo('App\Catalogo', 'rol');
    }


    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'correo_verificado_por' => 'datetime',
    ];
}
