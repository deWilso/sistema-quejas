<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Queja;
use App\catalogo;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {   
        $activos = DB::select("select count(*) as cantidad from quejas where estado_queja=71");
        $proceso = DB::select("select count(*) as cantidad from quejas where estado_queja=72");
        $quejas_activas = $activos[0]->cantidad;
        $quejas_proceso = $proceso[0]->cantidad;
        
        return view('home')->with('quejas_activas', $quejas_activas)
        ->with('quejas_proceso', $quejas_proceso);
    }

  
}
