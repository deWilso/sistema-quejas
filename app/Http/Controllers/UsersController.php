<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Catalogo;
class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuarios = User::all();
        return view ('usuarios.lista_usuario')->with('usuarios', $usuarios);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departamentos = Catalogo::where('id_catalogo_padre', 100)->get();
        $estado_civil = Catalogo::where('id_catalogo_padre', 30)->get();
        $genero = Catalogo::where('id_catalogo_padre', 40)->get();
        $sede_diaco = Catalogo::where('id_catalogo_padre', 50)->get();
        $roles = Catalogo::whereId_catalogo_padre(10)->get();
        


        return view ('usuarios.crear_usuario')->with('departamentos', $departamentos)
                                   ->with('estado_civil', $estado_civil)
                                   ->with('genero', $genero)
                                   ->with('sede_diaco', $sede_diaco)
                                   ->with('roles', $roles);
                                   
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // dd($request);
        $primer_nombre = $request->primer_nombre;
        $segundo_nombre = $request->segundo_nombre;
        $primer_apellido = $request->primer_apellido;
        $segundo_apellido = $request->segundo_apellido;
        $apellido_casada = $request->apellido_casada;
        $cui = $request->cui;
        $telefono = $request->telefono;
        $departamento = $request->departamento;
        $nacimiento = $request->fecha_nacimiento;
        $genero = $request->genero;
        $estado_civil = $request->estado_civil;
        $rol = $request->rol;
        $municipio = $request ->municipio;
        $direccion = $request->direccion;
        $sede = $request->sede;
        $correo = $request->usuario;    
        $password = $request->password;


        //guardar Consumidor
        $persona = new User;
        $persona->primer_nombre = $primer_nombre;
        $persona->segundo_nombre = $segundo_nombre;
        $persona->primer_apellido= $primer_apellido;
        $persona->segundo_apellido = $segundo_apellido;
        $persona->apellido_casada = $apellido_casada;
        $persona->departamento = $departamento;
        $persona->municipio = $municipio;
        $persona->direccion = $direccion;
        $persona->sede = $sede;    
        $persona->identificacion = $cui;
        $persona->telefono = $telefono;    
        $persona->fecha_nacimiento = $nacimiento;
        $persona->genero = $genero;
        $persona->estado_civil = $estado_civil;
        $persona->rol = $rol;
        $persona->email = $correo;
        $persona->password = bcrypt($password);
        $persona->save();
        return redirect(route('lista_usuarios'))->with('status', 'Usuario dado de alta con exito');          

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $usuario= User::whereId($id)->firstOrFail();
        $roles = Catalogo::whereId_catalogo_padre(10)->get();
        return view('usuarios.actualizar_usuario')->with('usuario', $usuario)->with('roles', $roles);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

      $usuario = User::whereId($id)->firstOrFail();
      $usuario->email = $request->get('usuario');
      $usuario->password = bcrypt($request->get('password'));
      $usuario->save();
      return redirect(action('UsersController@edit', $usuario->id))->with('status', 'Informacion del usuario actualizada con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $usuario = User::whereId($id)->firstOrFail();
        $usuario->delete();
        return redirect(route('lista_usuarios'))->with('status', 'La habitacion '.$usuario->primer_nombre.' ha sido dado de baja del sistema');

    }
}
