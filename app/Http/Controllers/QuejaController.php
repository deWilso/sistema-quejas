<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Catalogo;
use App\Persona;
use App\Proveedor;
use App\Queja;
use App\User;
use Illuminate\Support\Str;
use Mail;
use App\Mail\codigoQueja;
use Carbon\Carbon;
use Date;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
class QuejaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function administrativeHome(){
        $quejas = Queja::orderBy('fecha_creacion', 'DESC')->get();;
         return view ('queja-admin.quejas')->with('quejas', $quejas);
    }


    public function index()
    {
        $departamentos = Catalogo::where('id_catalogo_padre', 100)->get();
        $estado_civil = Catalogo::where('id_catalogo_padre', 30)->get();
        $genero = Catalogo::where('id_catalogo_padre', 40)->get();
        $sede_diaco = Catalogo::where('id_catalogo_padre', 50)->get();
        $nacionalidad = Catalogo::where('id_catalogo_padre', 60)->get();
        $tipoquejas = Catalogo::where('id_catalogo_padre', 20)->get();
        


        // dd($quejas_activas);

        return view ('queja.index')->with('departamentos', $departamentos)
                                   ->with('estado_civil', $estado_civil)
                                   ->with('genero', $genero)
                                   ->with('sede_diaco', $sede_diaco)
                                   ->with('nacionalidad', $nacionalidad)
                                   ->with('tipoquejas', $tipoquejas);
                            
    }

    public function sixMonths(Request $request){
        if ($request->ajax()){
            $datos;
            $julio = DB::Select("select count(*) as cantidad from quejas where fecha_creacion BETWEEN '2020-07-01' AND '2020-07-31'");
            $agosto = DB::Select("select count(*) as cantidad from quejas where fecha_creacion BETWEEN '2020-08-01' AND '2020-08-31'");
            $septiembre = DB::Select("select count(*) as cantidad from quejas where fecha_creacion BETWEEN '2020-09-01' AND '2020-09-31'");
            $octubre = DB::Select("select count(*) as cantidad from quejas where fecha_creacion BETWEEN '2020-10-01' AND '2020-10-31'");
            $noviembre = DB::Select("select count(*) as cantidad from quejas where fecha_creacion BETWEEN '2020-11-01' AND '2020-11-30'");
            $diciembre = DB::Select("select count(*) as cantidad from quejas where fecha_creacion BETWEEN '2020-12-01' AND '2020-12-30'");
            
            $datos = [$julio[0]->cantidad, $agosto[0]->cantidad,$septiembre[0]->cantidad,$octubre[0]->cantidad, $noviembre[0]->cantidad,$diciembre[0]->cantidad];


        }

        return Response ($datos); 

        

        // dd($datos);



    }




    public function getMunicipios(Request $request)
    {
        if($request->ajax())
        {
            $id=$request->id;
           
            $municipios= Catalogo::whereId_catalogo_padre($id)->get();
            return Response($municipios);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     * 
     * 
     */

     public function getViewSomeData(String $view)
     {
        $quejas = Queja::all();
        $sedes = Catalogo::whereId_catalogo_padre(50)->get();
        $estados = Catalogo::whereId_catalogo_padre(70)->get();
        $departamentos = Catalogo::whereId_catalogo_padre(100)->get();
        return view ($view)->with('quejas', $quejas)->with('sedes', $sedes)->with('estados', $estados)->with('departamentos', $departamentos);
     }
    public function filtroRangoFechaView()
    {
        return $this->getViewSomeData('queja-admin.filtroRangoFecha');
    }

    public function filtroRegionView(){
        return $this->getViewSomeData('queja-admin.filtroRegion');
    }

    public function quejaFiltrosFecha(Request $request){
        if($request->ajax()){
            $inicio = $request->inicio;
            $final = $request->fin;
            $queja_filtrada = Queja::whereBetween('fecha_creacion', [$inicio, $final])->get();
            $lenght = count($queja_filtrada);            
            for($i=0; $i<$lenght; $i++){
                $id_sede = $queja_filtrada[$i]->sede_queja;
                $id_proveedor = $queja_filtrada[$i]->proveedor_queja;
                $id_estado = $queja_filtrada[$i]->estado_queja;
                $sede = Catalogo::whereId_catalogo($id_sede)->pluck('descripcion');
                $proveedor = Proveedor::whereId($id_proveedor)->pluck('nombre');
                // dd($estado[0]);
                $estado = Catalogo::whereId_catalogo($id_estado)->pluck('descripcion');
                // dd($estado[0]);
                
                $queja_filtrada[$i]->sede_queja = $sede[0];
                $queja_filtrada[$i]->proveedor_queja = $proveedor[0];
                $queja_filtrada[$i]->estado_queja = $estado[0];
            }
            // dd($queja_filtrada);
            return Response($queja_filtrada);
        }
    }

    public function quejaFiltrosRegion(Request $request){
        if($request->ajax()){
            $sede = $request->sede;
            $region = $request->region;        
            
            $queja_filtrada = Queja::where('sede_queja', $sede)->get();
            $lenght = count($queja_filtrada);            
            for($i=0; $i<$lenght; $i++){
                $id_sede = $queja_filtrada[$i]->sede_queja;
                $id_proveedor = $queja_filtrada[$i]->proveedor_queja;
                $id_estado = $queja_filtrada[$i]->estado_queja;
                $sede = Catalogo::whereId_catalogo($id_sede)->pluck('descripcion');
                $proveedor = Proveedor::whereId($id_proveedor)->pluck('nombre');               
                $estado = Catalogo::whereId_catalogo($id_estado)->pluck('descripcion');           
                
                $queja_filtrada[$i]->sede_queja = $sede[0];
                $queja_filtrada[$i]->proveedor_queja = $proveedor[0];
                $queja_filtrada[$i]->estado_queja = $estado[0];
            }
      
            return Response($queja_filtrada);
        }


    }

    public function filtroDepartamentoView(){
        return $this->getViewSomeData('queja-admin.filtroDepartamento');
    }

    public function quejaFiltrosDepartamento(Request $request){
        if($request->ajax()){
            $depto = $request->depto;
            // $depto = 17000;
            
            $queja_filtrada =  DB::select('SELECT * FROM proveedores pro inner join quejas que ON pro.id = que.proveedor_queja where pro.departamento = '.$depto.'');
            $departamento = Catalogo::whereId_catalogo($depto)->pluck('descripcion');
            $depto_nombre = $departamento[0];
            // dd($depto_nombre);
            $lenght = count($queja_filtrada);   
            // dd($lenght);  
            if($lenght>=0){
                // $departamento = Catalogo::whereId_catalogo
                for($i=0; $i<$lenght; $i++){

                    $id_sede = $queja_filtrada[$i]->sede_queja;    
                    $id_depto = $queja_filtrada[$i]->departamento;
                    $id_estado = $queja_filtrada[$i]->estado_queja;
                    $sede = Catalogo::whereId_catalogo($id_sede)->pluck('descripcion');
                    $estado = Catalogo::whereId_catalogo($id_estado)->pluck('descripcion');         
                    
                    $queja_filtrada[$i]->sede_queja = $sede[0];
                    $queja_filtrada[$i]->estado_queja = $estado[0];
                    $queja_filtrada[$i]->departamento = $depto_nombre;

                }

            }                   
            
            // dd($queja_filtrada);
            return Response($queja_filtrada);


        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Datos del consumidor
    
        $primer_nombre = $request->primer_nombre;
        $segundo_nombre = $request->segundo_nombre;
        $primer_apellido = $request->primer_apellido;
        $segundo_apellido = $request->segundo_apellido;
        $apellido_casada = $request->apellido_casada;
        $cui = $request->cui;
        $telefono = $request->telefono;
        $nacionalidad = $request->nacionalidad;
        $departamento_consumidor = $request->departamento;
        $municipio_consumidor = $request ->municipio;
        $direccion_consumidor = $request->direccion;
        $genero = $request->genero;
        $estado_civil = $request->estado_civil;
        $sede = $request->sede;
        $correo = $request->correo;    

         //Datos del proveedor
         $nombre_negocio = $request->nombre_negocio;
         $nit_negogcio = $request->nit_negogcio;
         $direccion_negocio = $request->direccion_negocio;
         $departamento_proveedor = $request->departamento_proveedor;
         $municipio_proveedor = $request->municipio_proveedor;
 
        
         //Datos de la queja
        $tipo_queja = $request->tipo_queja;
        $descripcion_queja = $request->queja;
        $proposicion = $request->proposicion;
        $numero_queja = Str::random(6);
        $sede_queja = $request->sede;
        $estado_queja = 71;


        //guardar Consumidor
        $persona = new Persona;
        $persona->primer_nombre = $primer_nombre;
        $persona->segundo_nombre = $segundo_nombre;
        $persona->primer_apellido= $primer_apellido;
        $persona->segundo_apellido = $segundo_apellido;
        $persona->apellido_casada = $apellido_casada;
        $persona->departamento = $departamento_consumidor;
        $persona->municipio = $municipio_consumidor;
        $persona->direccion = $direccion_consumidor;
        $persona->identificacion = $cui;
        $persona->telefono = $telefono;
        $persona->nacionalidad = $nacionalidad;
        $persona->genero = $genero;
        $persona->estado_civil = $estado_civil;
        $persona->correo = $correo;
        $persona->save();

        
        
        //guardar Proveedor
        $proveedor = new Proveedor;
        $proveedor->nit = $nit_negogcio;
        $proveedor->nombre = $nombre_negocio;
        $proveedor->direccion = $direccion_negocio;
        $proveedor->departamento = $departamento_proveedor;
        $proveedor->municipio = $municipio_proveedor;
        $proveedor->save();

        //guardar queja 
        $queja = new Queja;
        $queja->numero_queja = $numero_queja;
        $queja->detalle_queja = $descripcion_queja;
        $queja->fecha_creacion = Date('Y-m-d');
        $queja->solicitud_consumidor = $proposicion;
        $queja->estado_queja = $estado_queja;
        $queja->tipo_queja = $tipo_queja;
        $queja->sede_queja = $sede_queja;
        $queja->persona_queja = $persona->id;
        $queja->proveedor_queja = $proveedor->id;
        $queja->save();    
        Mail::to($persona->correo)->send(new codigoQueja($persona, $queja));
        return redirect(route('quejas'))->with('status', 'Su queja ha sido recibida exitosamente');          

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getQueja(Request $request)
    {
        if($request->ajax())
        {
            $numero_queja=$request->numero_queja;
            
                       
            $detalle_queja= Queja::whereNumero_queja($numero_queja)->get();
            $id_estado = $detalle_queja[0]->estado_queja;
            $id_proveedor = $detalle_queja[0]->proveedor_queja;
            $descripcion = Catalogo::whereId_catalogo($id_estado)->pluck('descripcion');
            $proveedor = Proveedor::whereId($id_proveedor)->pluck('nombre');

            $detalle_queja[0]->estado_queja = $descripcion[0];      
            $detalle_queja[0]->proveedor_queja = $proveedor[0];   

            return Response($detalle_queja);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showQueja(){
        return view ('queja.queja');
    }
    public function edit($id)
    {   
        $queja = Queja::whereId($id)->firstOrFail();
       
        $id_proveedor = $queja->proveedor_queja;
        $estados = Catalogo::whereId_catalogo_padre(70)->get();
        $sedes = Catalogo::whereId_catalogo_padre(50)->get();
        $proveedor = Proveedor::whereId($id_proveedor)->get();
        $id_depto = $proveedor[0]->departamento;
        $id_municipio = $proveedor[0]->municipio;
        $muncipio = Catalogo::whereId_catalogo($id_municipio)->pluck('descripcion');
        $departamento = Catalogo::whereId_catalogo($id_depto)->pluck('descripcion');

        $queja[0] = Arr::add($queja, 'municipio', $muncipio, );
        $queja[0] = Arr::add($queja, 'departamento', $departamento);
        return view ('queja-admin.update', ['queja' => $queja])->with('estados', $estados)->with('sedes', $sedes);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //  dd($request);
        $queja = Queja::whereId($id)->firstOrFail();
      $queja->estado_queja = $request->get('estado_queja');
      $queja->sede_queja = $request->get('sede_queja');
      $queja->save();
      return redirect(action('QuejaController@edit', $queja->id))->with('status', 'Informacion de la queja actualizada con exito');
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
