<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    protected $table = 'personas';
    
        protected $fillable = [
            'primer_nombre', 'segundo_nombre','primer_apellido', 'segundo_apellido','apellido_casada','departamento','municipio','direccion','identificacion',
            'telefono', 'nacionalidad', 'genero','estado_civil','correo', 
        ];

        
    public function municipio ()
    {
     return $this->belongsTo('App\Catalogo', 'id_catalogo');
    }
    
    public function departamento()
    {
     return $this->belongsTo('App\Catalogo', 'id_catalogo');
    }
    

    
}
