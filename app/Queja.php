<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Queja extends Model
{
    
    protected $table = 'quejas';
    protected $fillable = [
        'numero_queja', 'detalle_queja', 'fecha_creacion', 'solicitud_consumidor', 'estado_queja', 'tipo_queja', 'sede_queja', 'persona', 'proveedor_queja'
    ];

    public  function estado (){
        return $this->belongsTo('App\Catalogo', 'estado_queja');
    }
    public  function tipo (){
        return $this->belongsTo('App\Catalogo', 'tipo_queja');
    }

    public  function proveedor(){
        return $this->belongsTo('App\Proveedor', 'proveedor_queja');
        
    }

    public  function sede(){
        return $this->belongsTo('App\Catalogo', 'sede_queja');
        
    }
    
  
  

    /**
     * Get all of the comments (single-level, not recursively)
     */

}

