<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proveedor extends Model
{
    protected $table = 'proveedores';

    protected $fillable = 
    [
        'nit', 'nombre', 'direccion', 'departamento', 'municipio'
    ];

    public function quejas () 
    {
        return $this->hasMany('App\Queja', 'id_queja');
    }
}
