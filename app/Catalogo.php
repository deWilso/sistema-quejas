<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Catalogo extends Model
{
    protected $primaryKey = 'id_catalogo';
    protected $table = 'catalogos';
    protected $fillable = [
        'id_catalogo', 'descripcion', 'id_catalogo_padre'
    ];
    public function quejas (){
        return $this->hasMany('App\Queja', 'id_queja');
    }


    
}

