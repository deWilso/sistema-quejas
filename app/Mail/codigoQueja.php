<?php

namespace App\Mail;


use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Persona;
use App\Queja;


class codigoQueja extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */



    public $persona;
    public $queja;
    public function __construct(Persona $persona, Queja $queja )
    {
        $this->persona = $persona;
        $this->queja = $queja;
        
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('numeroQueja')
                ->from('no-reply@diaco.com.gt')
                ->subject('Bienvenido!');
    }
}
