<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>DIACO Systems</title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="{{ asset('adminLte/plugins/fontawesome-free/css/all.min.css') }}">
  <!-- IonIcons -->
  <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('adminLte/css/adminlte.min.css')}}">
  <!-- datatables -->
  <link rel="stylesheet" href="{{asset('adminLte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{asset('adminLte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link rel="stylesheet" href="{{ asset('adminLte/plugins/select2/css/select2.min.css')}}">
  <link rel="stylesheet" href="{{ asset('adminLte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{ asset('adminLte/plugins/daterangepicker/daterangepicker.css')}}">
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to to the body tag
to get the desired effect
|---------------------------------------------------------|
|LAYOUT OPTIONS | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition sidebar-mini">

<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-dark navbar-dark">
    <!-- Left navbar links -->
   <!-- Left navbar links -->
   <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
     
    </ul>
         

      <!-- Notifications Dropdown Menu -->
      <ul class="navbar-nav ml-auto">
      <li class="nav-item dropdown">


      <li class="dropdown user user-menu">
            <!-- Menu Toggle Button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <!-- The user image in the navbar-->
              <img src="{{asset('adminLte/img/user2-160x160.jpg')}}" class="user-image" alt="User Image">
              <!-- hidden-xs hides the username on small devices so only the image appears. -->
              <span class="hidden-xs">{{ Auth::user()->primer_nombre  }} {{Auth::user()->primer_apellido}}</span>
            </a>
            <ul class="dropdown-menu">
              <!-- The user image in the menu -->
              <li class="user-header">
                <img src="{{asset('adminLte/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">

                <p>
                {{Auth::user()->primer_nombre}} {{Auth::user()->primer_apellido}}
                <?php
                    $user = Auth::user();
                    $typeuser = App\User::find($user->id)->rol_usuario()->first();
                  ?>
                  <small>{{$typeuser->descripcion}}</small>
                </p>
              </li>
              <!-- Menu Body -->
              
              <!-- Menu Footer-->
              <li class="user-footer">
                  <div class="pull-right">
                  <a href="{{ route('logout') }}" 
                  onclick="event.preventDefault();
                                    document.getElementById('frm-logout').submit();">
                  Salir
                  </a>
                    <form id="frm-logout" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </div>
              </li>
            </ul>
          </li>      
      </li>
     
    
  </nav>
  
  
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{route('home')}}" class="brand-link">
      <span class="brand-text font-weight-light">DIACO</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{asset('adminLte/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"> {{Auth::user()->primer_nombre}} {{Auth::user()->primer_apellido}}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview menu-open">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
            <li class="nav-item">
                <a href="{{route('home')}}" class="nav-link">
                <i class="far fa-chart-bar"></i>
                                  <p>Resumen</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('administrativeHome')}}" class="nav-link">
                <i class="fas fa-rss-square"></i>                  <p>Quejas</p>
                </a>
              </li>
              
            </ul>
          </li>

          <li class="nav-item has-treeview menu-open">
            <a href="#" class="nav-link active">
            <i class="fas fa-filter"></i>
               <p>
                Filtros
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
            <li class="nav-item">
                <a href="{{route('filtroRangoFechaView')}}" class="nav-link">
                <i class="fas fa-calendar-alt"></i>
                  <p>Por rango de fecha</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('filtroRegionView')}}" class="nav-link">
                <i class="fas fa-house-user"></i>            
                    <p>Por sede</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('filtroDepartamentoView')}}" class="nav-link">
                <i class="fas fa-city"></i>
                                  <p>Por Departamento</p>
                </a>
              </li>
            </ul>
          </li>
          @if(Auth::user()->rol==12)
          <li class="nav-item has-treeview menu-open">
            <a href="#" class="nav-link active">
            <i class="fas fa-users"></i>
                          <p>
                Usuarios y Roles
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
            <li class="nav-item">
                <a href="{{route('lista_usuarios')}}" class="nav-link">
                <i class="fas fa-user-cog"></i>             
                    <p>Listar/Crear</p>
                </a>
              </li>
            </ul>
          </li>
          @endif
    </div>
    <!-- /.sidebar -->
  </aside>

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
      @yield('title')
        <small>@yield('description')</small>
      </h1>
      @yield('breadcrumb')
      
    </section>

    <!-- Main content -->
    <section class="content container-fluid">

      <!--------------------------
        | Your Page Content Here |
        -------------------------->
        @yield('content')
    </section>
    <!-- /.content -->
  </div> <!-- content wraper -->
<!--  -->
  <!-- Main Footer -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">DIACO Systems</a>.</strong>
    Todos los derechos reservados.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 1.0
    </div>
  </footer>
 
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
    <script src="{{ asset('adminLte/plugins/jquery/jquery.min.js')}}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset('adminLte/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <!-- AdminLTE -->
    <script src="{{ asset('adminLte/js/adminlte.js')}}"></script>
    <!-- OPTIONAL SCRIPTS -->
    <script src="{{ asset('adminLte/plugins/chart.js/Chart.min.js')}}"></script>
    <script src="{{ asset('adminLte/js/demo.js')}}"></script>
    <script src="{{ asset('adminLte/js/pages/dashboard3.js')}}"></script>


    <!-- datatables -->
    <script src="{{asset('adminLte/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('adminLte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('adminLte/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('adminLte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<script src="{{asset('adminLte/plugins/moment/moment.min.js')}}"></script>
<script src="{{asset('adminLte/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js')}}"></script>
    @yield('js')
</body>
</html>
