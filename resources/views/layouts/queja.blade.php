<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link href="https://cdn.jsdelivr.net/npm/bs-stepper/dist/css/bs-stepper.min.css">
    
   
    
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
 
    <link rel="stylesheet" href="{{asset('styleStepper.css')}}">
    
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="{{route('quejas')}}">Inicio <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{route('showQueja')}}">Ver queja</a>
      </li>
    </ul>
    <div class="form-inline my-2 my-lg-0">
    
        <a class="btn btn primary" style="color: white"href="{{Route('login')}}">Iniciar Sesion</a>
        

      
      
  </div>
</nav>
<div class="container">
@yield('content')

</div>
    
</body>
@yield ('js')

<!-- jQuery -->
<script src="{{ asset('adminLte/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap -->
<script src="{{ asset('adminLte/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<link href='https://cdn.jsdelivr.net/npm/bs-stepper/dist/js/bs-stepper.min.js'>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>

</html>