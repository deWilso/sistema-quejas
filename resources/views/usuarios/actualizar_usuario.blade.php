@extends ('layouts.app')


@section('title')
Usuario
  
@stop

@section('description')
    Actualizar informacion del usuario
@stop

@section('breadcrumb')
                
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('lista_usuarios')}}">Actualizar Usuario</a></li>
                        <li class="breadcrumb-item active">Actualizar informacion del usuario</li>
                    </ol>
              
@stop

@section('content')




<div class="card card-default  mt-3">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
        <form class="form-horizontal" method="post">
             {{csrf_field()}}
          <!-- /.card-header -->
          <div class="card-body">
            <div class="row">
              <div class="col-md-6">
                <div class="card-header mb-3">
                  <h3 class="card-title">Informacion del usuario</h3>
                </div>
                <div class="form-group row">
               
                  <label class="col-sm-4 col-form-label">Nombre</label>
                  <div class="col-sm-8">
                  <input type="text" class="form-control" disabled="disabled" name="nombres"value="{!!$usuario->primer_nombre.' '.$usuario->segundo_nombre.' '.$usuario->primer_apellido.' '.$usuario->segundo_apellido!!}">
                  </div>
                </div>
                <div class="form-group row">
               
                  <label class="col-sm-4 col-form-label">DPI</label>
                  <div class="col-sm-8">
                  <input type="text" class="form-control" disabled="disabled" name="dpi"value="{!!$usuario->identificacion!!}">
                  </div>
                </div>
                
        
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              <div class="col-md-6">
                <div class="card-header mb-3">
                  <h3 class="card-title">Informacion de Acceso</h3>
                </div>
                <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Cambiar Rol</label>
                    <div class="col-sm-8">
                      <select class="form-control select2"  name="estado_queja" style="width: 100%;">
                        <option value="0">--seleccione el rol--</option>         
                        @foreach($roles as $rol)     
                        <option value="{{$rol->id_catalogo}}" {{$usuario->rol == $rol->id_catalogo ?  'selected'  : ''}}>{{ $rol->descripcion }}</option>         
                        @endforeach

                      </select>
                    </div>              
                  
                </div>
              
                <div class="form-group row">
                  <label class="col-sm-4 col-form-label">Usuario</label>
                  <div class="col-sm-8">
                  <input type="text" class="form-control"  name="usuario" id="usuario" value="{{$usuario->email}}">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-4 col-form-label">Contrasenia</label>
                  <div class="col-sm-8">
                  <input type="password" class="form-control"  name="password" id="password" value="{{$usuario->password}}">
                  </div>
                </div>
              <!-- /.col -->
            </div>
          </div>
          <!-- /.card-body -->
          <div class="box-footer" style="margin-top: 15px; text-align: center">
             <a href="{{route('lista_usuarios')}}" class="btn btn-danger">Regresar</a>
              <button type="submit" class="btn btn-info">Actualizar</button>
              
            </div>
          <!-- /.row -->
        </form>
          
         
 </div>
        <!-- /.card -->

@endsection
 @section('js')
<script type="text/javascript">
 
      $('div.alert').delay(5000).slideUp(300);
     

</script>
@endsection
