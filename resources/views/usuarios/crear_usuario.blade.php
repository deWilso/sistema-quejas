@extends ('layouts.app')


@section('title')
Usuario
  
@stop

@section('description')
    Actualizar informacion del usuario
@stop

@section('breadcrumb')
                
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('lista_usuarios')}}">Actualizar Usuario</a></li>
                        <li class="breadcrumb-item active">Actualizar informacion del usuario</li>
                    </ol>
              
@stop

@section('content')




<div class="card card-default  mt-3">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
        <form class="form-horizontal" method="post" action="{{route('saveUser')}}">
             {{csrf_field()}}
          <!-- /.card-header -->
          <div class="card-body">
            <div class="row">
              <div class="col-md-6">
                <div class="card-header mb-3">
                  <h3 class="card-title">Informacion del usuario</h3>
                </div>
                <div class="multisteps-form__panel shadow p-4 rounded bg-white js-active" data-animation="scaleOut">
    
                                                <div class="multisteps-form__content">
                                                    <div class="form-row mt-4">
                                                        <div class="col-12 col-sm-6">
                                                            <input class="multisteps-form__input form-control" name="primer_nombre" type="text" placeholder="Primer Nombre" />
                                                        </div>
                                                        <div class="col-12 col-sm-6 mt-4 mt-sm-0">
                                                            <input class="multisteps-form__input form-control" name="segundo_nombre" type="text" placeholder="Segundo Nombre" />
                                                        </div>
                                                    </div>
                                                    <div class="form-row mt-4">
                                                        <div class="col-12 col-sm-6">
                                                            <input class="multisteps-form__input form-control" name="primer_apellido" type="text" placeholder="Primer Apellido" />
                                                        </div>
                                                        <div class="col-12 col-sm-6 mt-4 mt-sm-0">
                                                            <input class="multisteps-form__input form-control" name="segundo_appellido" type="text" placeholder="Segundo Apellido" />
                                                        </div>
                                                    </div>
                                                    <div class="form-row mt-4">
                                                        <div class="col-12 col-sm-6">
                                                            <input class="multisteps-form__input form-control" name="apellido_casada" type="text" placeholder="Apellido de Casada" />
                                                        </div>
                                                        <div class="col-12 col-sm-6">
                                                            <input class="multisteps-form__input form-control" name="cui" type="text" placeholder="DPI" />
                                                        </div>
                                                    </div>
                                                    <div class="form-row mt-4">
                                                        <div class="col-12 col-sm-6">
                                                            <input class="multisteps-form__input form-control" name="telefono" type="text" placeholder="Telefono" />
                                                        </div>
                                                        <div class="col-12 col-sm-6">
                                                            <input class="multisteps-form__input form-control" name="fecha_nacimiento" type="date" />
                                                        </div>
                                                       
                                                    </div>
                                                    <div class="form-row mt-4">
                                                        <div class="col-12 col-sm-6">
                                                            <select class="form-control" name="departamento" id="departamento"> 
                                                            <option value="0">--Departamento--</option>
                                                                @foreach($departamentos as $departamento)


                                                                 <option value="{{ $departamento->id_catalogo}}">{{ $departamento->descripcion}}</option>
                                                                 @endforeach
                                                                 </select>
            
                                                        </div>

                                                        <div class="col-12 col-sm-6">
                                                            <select class="form-control" name="municipio" id="municipio"> 
                                                <option value="0">--Municipio--</option>
                                               
                                              </select>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    

                                                    <div class="form-row mt-4">

                                                       
                                                        <div class="col-12 col-sm-6">
                                                            <select class="form-control" name="sede" id="sede"> 
                                                            <option value="0">--Sede--</option>
                                                                @foreach($sede_diaco as $sede_dia)


                                                                 <option value="{{ $sede_dia->id_catalogo}}">{{ $sede_dia->descripcion}}</option>
                                                                 @endforeach
                                             
                                                            </select>
                                                        </div>

                                                        <div class="col-12 col-sm-6">
                                                            <input class="multisteps-form__input form-control" name="direccion" type="text" placeholder="Direccion" />
                                                        </div>
                                                    </div>

                                                    <div class="form-row mt-4">

                                                       
                                                        <div class="col-12 col-sm-6">
                                                            <select class="form-control" name="genero" id="genero"> 
                                                            <option value="0">--Genero--</option>
                                                                @foreach($genero as $gen)


                                                                 <option value="{{ $gen->id_catalogo}}">{{ $gen->descripcion}}</option>
                                                                 @endforeach
                                             
                                                            </select>
                                                        </div>

                                                        <div class="col-12 col-sm-6">
                                                             <select class="form-control" name="estado_civil" id="estado_civil"> 
                                                            <option value="0">--Estado_civil--</option>
                                                                @foreach($estado_civil as $est)


                                                                 <option value="{{ $est->id_catalogo}}">{{ $est->descripcion}}</option>
                                                                 @endforeach
                                             
                                                            </select>  
                                                         </div>
                                                    </div>
                                                    
                                                    
                                                </div>
                                            </div>
                
        
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              <div class="col-md-6">
                <div class="card-header mb-3">
                  <h3 class="card-title">Informacion de Acceso</h3>
                </div>
                <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Cambiar Rol</label>
                    <div class="col-sm-8">
                      <select class="form-control select2"  name="rol" style="width: 100%;">
                        <option value="0">--seleccione el rol--</option>         
                        @foreach($roles as $rol)     
                        <option value="{{$rol->id_catalogo}}">{{ $rol->descripcion }}</option>         
                        @endforeach

                      </select>
                    </div>              
                  
                </div>
              
                <div class="form-group row">
                  <label class="col-sm-4 col-form-label">Usuario</label>
                  <div class="col-sm-8">
                  <input type="text" class="form-control"  name="usuario" id="usuario" value="">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-4 col-form-label">Contrasenia</label>
                  <div class="col-sm-8">
                  <input type="password" class="form-control"  name="password" id="password" value="">
                  </div>
                </div>
              <!-- /.col -->
            </div>
          </div>
          <!-- /.card-body -->
          <div class="box-footer" style="margin-top: 15px; text-align: center">
             <a href="{{route('lista_usuarios')}}" class="btn btn-danger">Regresar</a>
              <button type="submit" class="btn btn-info">Dar alta</button>
              
            </div>
          <!-- /.row -->
        </form>
          
         
 </div>
        <!-- /.card -->

@endsection
 @section('js')
<script type="text/javascript">
 
      $('div.alert').delay(5000).slideUp(300);
      $('#departamento').on('change', function() 
	{
	  
	   		var id = $(this).val();   
            console.log(id);
			
			$.ajax
			({
				type : 'get',
				url : '{{URL::to('getMunicipios')}}',
				data:{'id':id},
				success:function(data)
				{
                    $("#municipio").empty();
                    console.log(data);
                    for (var i=0; i<data.length;i++)
                    {
                    
                    $("#municipio").append("<option value='" + data[i].id_catalogo+"'>"+data[i].descripcion+"</option>");
                                
                    }
                
				}
			});
			
   });
     

</script>
@endsection