@extends('layouts.app')
@section('title')
Usuarios
@stop
@section('description')
Usuarios y roles del sistema
@stop

@section ('breadcrumb')
<ol class="breadcrumb float-sm-right">
    <li class="breadcrumb-item"><a href="{{route('administrativeHome')}}">Lista de Usuarios</a></li>
    <li class="breadcrumb-item active">Usuarios</li>
</ol>              
@stop
@section('content')

<div class="card mt-3">
          @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
              
              <!-- /.card-header -->
              <div class="card-body">
              <div class="row">
              <a href="{!! action('UsersController@create') !!}"  type="button"class="btn btn-primary">Agregar Usuario</a>

              </div>
                <table id="tabla-quejas" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Nombres </th>
                    <th>DPI</th>
                    <th>Fecha de Nacimiento</th>
                    <th>Telefono</th>
                    <th>Direccion</th>
                    <th>Correo / Usuario</th>  
                    <th>Rol</th>      
                    <th>Accion</th>             
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($usuarios as $usuario)
                  <tr>
                      <td>{{$usuario->primer_nombre.' '.$usuario->segundo_nombre.' '.$usuario->primer_apellido.' '.$usuario->segundo_apellido}}</td>
                      <td>{{$usuario->identificacion}}</td>
                      <td>{{$usuario->fecha_nacimiento}}</td>
                      <td>{{$usuario->telefono}}</td>
                      <td>{{$usuario->direccion .', '.$usuario->muni_usuario->descripcion .', '.$usuario->depto_usuario->descripcion}}</td>
                      <td>{{$usuario->email}}</td>
                      <td>{{$usuario->rol_usuario->descripcion}}</td>
                      <td>
                      <form method="post" action="{!! action('UsersController@destroy', $usuario->id) !!}" class="pull-right">
                                    {!! csrf_field() !!}
                                    <div>
                                        <button type="submit" class="btn btn-danger">Dar de Baja</button>
                                    </div>                
                                </form>

                      <a href="{!! action('UsersController@edit', $usuario->id) !!}"  type="button"class="btn btn-primary">Editar</a>

                      </td>
                  </tr>
                  @endforeach
                
                  </tbody>
                </table>
                       <!-- /.card-body -->
            </div>



@stop
@section('js')

<script type="text/javascript">
 $('div.alert').delay(5000).slideUp(300);
$('#filtrar').click(function(){
  $sede = $('#sede_queja').val();
  $region = $('#region_queja').val();
 
  $.ajax({
    type: 'get',
    url: '{{URL::to('quejaFiltrosRegion')}}',
    data: {'sede': $sede, 'region': $region},
    success: function (response){
      console.log(response);
      $('#tabla-quejas').dataTable().fnClearTable();
      $('#tabla-quejas').dataTable().fnDraw();
      $('#tabla-quejas').dataTable().fnDestroy();
      for(var i=0; i<response.length; i++){
        $('#tabla-quejas').append('<tr><td>' + response[i].numero_queja + '</td><td>' + response[i].fecha_creacion + '</td><td>' + response[i].sede_queja  + '</td><td>'+response[i].detalle_queja+ '</td><td>' + response[i].solicitud_consumidor + '</td><td>'+response[i].proveedor_queja + '</td><td>'+response[i].estado_queja+'</td></tr>');

      }         

    }

  });
  
});


$(function () {
    $("#tabla-quejas").DataTable({
      "responsive": true,
      "autoWidth": false,

      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
    });


  });
   
    

  

</script>
@endsection