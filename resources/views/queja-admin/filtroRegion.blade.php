@extends('layouts.app')
@section('title')
Filtros de quejas
@stop
@section('description')
Filtrar quejas por fechas 
@stop

@section ('breadcrumb')
<ol class="breadcrumb float-sm-right">
    <li class="breadcrumb-item"><a href="{{route('administrativeHome')}}">Filtro de quejas</a></li>
    <li class="breadcrumb-item active">Filtros</li>
</ol>
              
@stop
@section('content')
<div class="row">
<div class="form-group col">
                  <label class="col-sm-4 col-form-label">Sede</label>
                  <div class="col-sm-8">
                  <select class="form-control select2"  id="sede_queja"name="sede_queja" style="width: 100%;">
                    <option value="0">--Seleccione sede-</option>         
                    @foreach($sedes as $sede)     
                    <option value="{{$sede->id_catalogo}}">{{$sede->descripcion}}</option>         
                    @endforeach
                  </select>
                  </div>        
</div>

<div class="form-group col">      
              <input type="button" class="form-control mt-4  btn btn-primary"  name="filtrar" id="filtrar" value="filtrar">               
</div>
</div>


<div class="card mt-3">
              
              <!-- /.card-header -->
              <div class="card-body">
                <table id="tabla-quejas" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Numero de queja</th>
                    <th>Fecha de creacion</th>
                    <th>Sede</th>
                    <th>Detalle</th>
                    <th>Solicitud</th>
                    <th>Negocio</th>
                    <th>Estado</th>                    
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($quejas as $queja)
                  <tr>
                      <td>{{$queja->numero_queja}}</td>
                      <td>{{$queja->fecha_creacion}}</td>
                      <td>{{$queja->sede->descripcion}}</td>
                      <td>{{$queja->detalle_queja}}</td>
                      <td>{{$queja->solicitud_consumidor}}</td>
                      <td>{{$queja->proveedor->nombre}}</td>
                      <td>{{$queja->estado->descripcion}}</td>
                  </tr>
                  @endforeach
                
                  </tbody>
                </table>
                       <!-- /.card-body -->
            </div>



@stop
@section('js')

<script type="text/javascript">

$('#filtrar').click(function(){
  $sede = $('#sede_queja').val();
  $region = $('#region_queja').val();
 
  $.ajax({
    type: 'get',
    url: '{{URL::to('quejaFiltrosRegion')}}',
    data: {'sede': $sede, 'region': $region},
    success: function (response){
      console.log(response);
      $('#tabla-quejas').dataTable().fnClearTable();
      $('#tabla-quejas').dataTable().fnDraw();
      $('#tabla-quejas').dataTable().fnDestroy();
      for(var i=0; i<response.length; i++){
        $('#tabla-quejas').append('<tr><td>' + response[i].numero_queja + '</td><td>' + response[i].fecha_creacion + '</td><td>' + response[i].sede_queja  + '</td><td>'+response[i].detalle_queja+ '</td><td>' + response[i].solicitud_consumidor + '</td><td>'+response[i].proveedor_queja + '</td><td>'+response[i].estado_queja+'</td></tr>');

      }         

    }

  });
  
});


$(function () {
    $("#tabla-quejas").DataTable({
      "responsive": true,
      "autoWidth": false,

      "paging": true,
      "lengthChange": true,
      "searching": false,
      "ordering": true,
      "info": true,
    });


  });
   
    

  

</script>
@endsection