@extends ('layouts.app')


@section('title')
Detalle de quejas
  
@stop

@section('description')
    Filtro y lista de las quejas realizadas por sector
@stop

@section('breadcrumb')
                
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('administrativeHome')}}">Quejas</a></li>
                        <li class="breadcrumb-item active">Lista de quejas</li>
                    </ol>
              
@stop

@section('content')

<!-- --------------------- -->
<div class="card mt-3">
              
           
              <!-- /.card-header -->
              <div class="card-body">
                <table id="tabla-quejas" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Numero de queja</th>
                    <th>Fecha de creacion</th>
                    <th>Sede</th>
                    <th>Detalle</th>
                    <th>Solicitud</th>
                    <th>Negocio</th>
                    <th>Estado</th>                    
                    <th>Accion</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($quejas as $queja)
                  <tr>
                    <td>{{$queja->numero_queja}}</td>
                    <td>{{$queja->fecha_creacion}}</td>
                    <td>{{$queja->sede->descripcion}}</td>
                    <td>{{$queja->detalle_queja}}</td>
                    <td>{{$queja->solicitud_consumidor}}</td>
                    <td>{{$queja->proveedor->nombre}}</td>
                    <td>{{$queja->estado->descripcion}}</td>
                    <td>
                        <a href="{!! action('QuejaController@edit', $queja->id) !!}"  type="button"class="btn btn-primary">Gestionar</a>
                    </td>
                  </tr>
                  @endforeach
                  </tbody>
                  <tfoot>
                  <tr>
                  <th>Numero de queja</th>
                    <th>Fecha de creacion</th>
                    <th>Sede</th>
                    <th>Detalle</th>
                    <th>Solicitud</th>
                    <th>Negocio</th>
                    <th>Estado</th>                    
                    <th>Accion</th>
                  </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>



@endsection

<!-- ---------------------------- -->
@section('js')

<script type="text/javascript">
// $(document).ready( function () {
//     $('#example1').DataTable();
// } );

$(function () {
    $("#tabla-quejas").DataTable({
      "responsive": true,
      "autoWidth": true,

      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
    });
  });
</script>
@endsection


