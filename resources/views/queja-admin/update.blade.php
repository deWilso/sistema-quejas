@extends('layouts.app')

@section('title')
Gestionar queja
@stop

@section('description')
Cambiar estado de queja
@stop

@section('breadcrumb')
                
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('administrativeHome')}}">Lista de quejas</a></li>
                        <li class="breadcrumb-item active">Gestion de queja</li>
                    </ol>
              
@stop

@section ('content')
<div class="card card-default  mt-3">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
        <form class="form-horizontal" method="post">
             {{csrf_field()}}
          <!-- /.card-header -->
          <div class="card-body">
            <div class="row">
              <div class="col-md-6">
                <div class="card-header mb-3">
                  <h3 class="card-title">Datos de la queja</h3>
                </div>
              

                <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Estado de la queja</label>
                    <div class="col-sm-8">
                      <select class="form-control select2"  name="estado_queja" style="width: 100%;">
                        <option value="0">Cambiar estado de la queja</option>         
                        @foreach($estados as $estado)     
                        <option value="{{$estado->id_catalogo}}" {{$queja->estado_queja == $estado->id_catalogo ?  'selected'  : ''}}>{{ $estado->descripcion }}</option>         
                        @endforeach

                      </select>
                    </div>              
                  
                </div>
                <div class="form-group row">
                  <label class="col-sm-4 col-form-label">Sede asignada</label>
                  <div class="col-sm-8">
                  <select class="form-control select2"  name="sede_queja" style="width: 100%;">
                    <option value="0">Cambiar sede</option>         
                    @foreach($sedes as $sede)     
                    <option value="{{$sede->id_catalogo}}" {{$queja->sede_queja == $sede->id_catalogo ?  'selected'  : ''}}>{{ $sede->descripcion }}</option>         
                    @endforeach

                  </select>
                    </div>              
                  
                </div>
                
                <div class="form-group row">
               
                  <label class="col-sm-4 col-form-label">Fecha de creacion</label>
                  <div class="col-sm-8">
                  <input type="text" class="form-control" disabled="disabled" name="fecha_creacion"value="{!!$queja->fecha_creacion!!}">
                  </div>
                </div>
                
                <!-- /.form-group -->
                <div class="form-group row">
                  <label class="col-sm-4 col-form-label">Detalle de la queja</label>
                  <div class="col-sm-8">
                  <textarea class="form-control" disabled="disabled" name="detalle_queja" id="detalle_queja" cols="30" rows="2">{{$queja->detalle_queja}}</textarea>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-4 col-form-label">Solicitud del consumidor</label>
                  <div class="col-sm-8">
                  <textarea class="form-control" disabled="disabled" name="solicitud_consumidor"  id="solicitud_consumidor" cols="30" rows="2">{{$queja->solicitud_consumidor}}</textarea>
                  </div>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              <div class="col-md-6">
                <div class="card-header mb-3">
                  <h3 class="card-title">Datos del proveedor</h3>
                </div>
                <div class="form-group row">
                  <label class="col-sm-4 col-form-label">Nombre del proveedor</label>
                  <div class="col-sm-8">
                  <input type="text" class="form-control" disabled="disabled" name="nombre_proveedor" id="nombre_proveedor" value="{{$queja->proveedor->nombre}}">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-4 col-form-label">Nit del proveedor</label>
                  <div class="col-sm-8">
                  <input type="text" class="form-control" disabled="disabled" name="nit_proveedor" id="nit_proveedor" value="{{$queja->proveedor->nit}}">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-4 col-form-label">Direccion</label>
                  <div class="col-sm-8">
                  <input type="text" class="form-control" disabled="disabled" name="direccion_proveedor" id="direccion_proveedor" value="{{$queja->proveedor->direccion.', '.$queja->municipio[0].', '.$queja->departamento[0]}}">
                  </div>
                </div>
              <!-- /.col -->
            </div>
          </div>
          <!-- /.card-body -->
          <div class="box-footer" style="margin-top: 15px; text-align: center">
             <a href="{{route('administrativeHome')}}" class="btn btn-danger">Regresar</a>
              <button type="submit" class="btn btn-info">Actualizar</button>
              
            </div>
          <!-- /.row -->
        </form>
          
         
 </div>
        <!-- /.card -->

@endsection
 @section('js')
<script type="text/javascript">
 
      $('div.alert').delay(5000).slideUp(300);
     

</script>
@endsection
