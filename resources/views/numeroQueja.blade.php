<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
</head>
<body>
    Estimad@ {{$persona->primer_nombre.' '.$persona->primer_apellido}}, hemos recibido su queja satisfactoriamente, estos datos no seran revelados publicamente
    quedando totalmente a discrecion.
    Le enviamos el numero de su queja, para que usted pueda ver el estado de la misma a traves del siguiente enlace
    http://miopinioncuenta.herokuapp.com/showQueja

    El numero de su queja es <strong>{{$queja->numero_queja}} </strong>

    
</body>
</html>