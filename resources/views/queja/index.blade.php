@extends('layouts.queja')


@section('content')
<hr>

            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
                    <div class="content__inner">

                        <div class="container overflow-hidden">
                            <div class="multisteps-form">
                                <div class="row">
                                    <div class="col-12 col-lg-8 ml-auto mr-auto mb-4">
                                        <div class="multisteps-form__progress">
                                            <button class="multisteps-form__progress-btn js-active" type="button" title="User Info">Informacion Personal</button>
                                            <button class="multisteps-form__progress-btn" type="button" title="Address">Proveedor/Negocio</button>
                                            <button class="multisteps-form__progress-btn" type="button" title="Order Info">Queja</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 col-lg-8 m-auto">
                                        <form class="multisteps-form__form" id="formulario"method="POST" action="{{route('storeQueja')}}">
                                        @csrf
                                            <div class="multisteps-form__panel shadow p-4 rounded bg-white js-active" data-animation="scaleOut">
                                                <h3 class="multisteps-form__title">Llene la informacion solicitada </h3>
                                                <h5 class="multisteps-form__title" style="color:red">Los campos con (*) son obligatorios </h5>
                                                <div class="multisteps-form__content">
                                                    <div class="form-row mt-4">
                                                        <div class="col-12 col-sm-6">
                                                        <input class="multisteps-form__input form-control" id="primer_nombre" name="primer_nombre" type="text" placeholder="Primer Nombre (*)">
                                                        </div>
                                                        <div class="col-12 col-sm-6 mt-4 mt-sm-0">
                                                            <input class="multisteps-form__input form-control" name="segundo_nombre" type="text" placeholder="Segundo Nombre " />
                                                        </div>
                                                    </div>
                                                    <div class="form-row mt-4"> 
                                                        <div class="col-12 col-sm-6">
                                                        <input class="multisteps-form__input form-control"  name="primer_apellido" type="text" placeholder="Primer Apellido (*)" />
                                                        </div>
                                                        <div class="col-12 col-sm-6 mt-4 mt-sm-0">
                                                            <input class="multisteps-form__input form-control" name="segundo_appellido" type="text" placeholder="Segundo Apellido" />
                                                        </div>
                                                    </div>
                                                    <div class="form-row mt-4">
                                                        <div class="col-12 col-sm-6">
                                                            <input class="multisteps-form__input form-control" name="apellido_casada" type="text" placeholder="Apellido de Casada" />
                                                        </div>
                                                        <div class="col-12 col-sm-6">
                                                            <input class="multisteps-form__input form-control" name="cui" type="text" placeholder="DPI" />
                                                        </div>
                                                    </div>
                                                    <div class="form-row mt-4">
                                                        <div class="col-12 col-sm-6">
                                                            <input class="multisteps-form__input form-control" name="telefono" type="text" placeholder="Telefono" />
                                                        </div>
                                                        <div class="col-12 col-sm-6">
                                                        <select class="form-control" name="nacionalidad" id="nacionalidad"> 
                                                            <option value="0">--Nacionalidad (*)--</option>
                                                            @foreach($nacionalidad as $nacion)


                                                        <option value="{{ $nacion->id_catalogo}}">{{ $nacion->descripcion}}</option>
                                                        @endforeach
                                              
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-row mt-4">
                                                        <div class="col-12 col-sm-6">
                                                        <select class="form-control" name="departamento" id="departamento"> 
                                                            <option value="0">--Departamento (*)--</option>
                                                                @foreach($departamentos as $departamento)


                                                                 <option value="{{ $departamento->id_catalogo}}">{{ $departamento->descripcion}}</option>
                                                                 @endforeach
                                                                 </select>
            
                                                        </div>

                                                        <div class="col-12 col-sm-6">
                                                        <select class="form-control" name="municipio" id="municipio"> 
                                                <option value="0">--Municipio (*)--</option>
                                               
                                              </select>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-row mt-4">
                                                        <div class="col-12 col-sm-6">
                                                             <input class="multisteps-form__input form-control" name="direccion" type="text" placeholder="Direccion" />
                                                        </div>
                                                        <div class="col-12 col-sm-6">
                                                            <select class="form-control" name="genero" id="genero"> 
                                                            <option value="0">--Genero (*)--</option>
                                                            @foreach($genero as $gen)


                                                                 <option value="{{ $gen->id_catalogo}}">{{ $gen->descripcion}}</option>
                                                                 @endforeach
                                               
                                                        
                                                            </select>
                                                        </div>

                                                    </div>

                                                    <div class="form-row mt-4">

                                                        <div class="col-12 col-sm-6">
                                                            <select class="form-control" name="estado_civil" id="estado_civil"> 
                                                            <option value="0">--Estado Civil (*)--</option>
                                                            @foreach($estado_civil as $estado)


                                                                 <option value="{{ $estado->id_catalogo}}">{{ $estado->descripcion}}</option>
                                                                 @endforeach
                                                    
                                                    
                                                            </select>
                                                        </div>
                                                        <div class="col-12 col-sm-6">
                                                            <select class="form-control" name="sede" id="sede"> 
                                                            <option value="0">--Sede DIACO mas cercana (*)--</option>
                                                                @foreach($sede_diaco as $sede_dia)


                                                                 <option value="{{ $sede_dia->id_catalogo}}">{{ $sede_dia->descripcion}}</option>
                                                                 @endforeach
                                             
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-row mt-4">
                                                        
                                                        <div class="col-12 col-sm-12">
                                                            <input class="multisteps-form__input form-control" name="correo" type="mail" placeholder="Correo Electronico (*)" />
                                                        </div>
                                                    </div>

                                                    <div class="button-row d-flex mt-4">
                                                        <button class="btn btn-primary ml-auto js-btn-next" id="datos_person" type="button" title="Next">Siguiente</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- fin step informacion personal -->

                                            <div class="multisteps-form__panel shadow p-4 rounded bg-white" data-animation="scaleOut">
                                                <h3 class="multisteps-form__title">Llene la informacion del Negocio</h3>
                                                <h5 class="multisteps-form__title" style="color:red">Los campos con (*) son obligatorios </h5>

                                                <div class="multisteps-form__content">
                                                    <div class="form-row mt-4">
                                                    
                                                        <div class="col-12 col-sm-12">
                                                            <input class="multisteps-form__input form-control" name="nombre_negocio" type="text" placeholder="Escriba el nombre del Negocio (*)" />

                                                        </div>

            
                                                     </div>

                                                     <div class="form-row mt-4">
                                                        <div class="col-12 col-sm-6">
                                                            <input class="multisteps-form__input form-control" name="nit_negogcio" type="text" placeholder="Nit del Negocio" />
                                                        </div>
                                                        <div class="col-12 col-sm-6">
                                                            <input class="multisteps-form__input form-control"  name="direccion_negocio" type="text" placeholder=" Direccion del Negocio (*)" />
                                                        </div>

                                                    </div>
                                                    <div class="form-row mt-4">
                                                    
                                                        <div class="col-12 col-sm-6">
                                                            <select class="form-control" name="departamento_proveedor" id="departamento_proveedor"> 
                                                                <option value="0">--Departamento (*)--</option>
                                                                @foreach($departamentos as $departamento)
                                                                <option value="{{ $departamento->id_catalogo}}">{{ $departamento->descripcion}}</option>
                                                                @endforeach


                                                            </select>
                                                        </div>

                                                        <div class="col-12 col-sm-6">
                                                            <select class="form-control" name="municipio_proveedor" id="municipio_proveedor"> 
                                                                 <option value="0">--Municipio (*) --</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                   
                                                    <div class="button-row d-flex mt-4">
                                                        <button class="btn btn-primary js-btn-prev" type="button" title="Prev">Anterior</button>
                                                        <button class="btn btn-primary ml-auto js-btn-next" type="button" title="Next">Siguiente</button>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="multisteps-form__panel shadow p-4 rounded bg-white" data-animation="scaleIn">
                                                
                                                <h3 class="multisteps-form__title">Detalles de la queja</h3>
                                            <h5 class="multisteps-form__title" style="color:red">Los campos con (*) son obligatorios </h5>

                                                <div class="multisteps-form__content form-group">      
                                                            <div class="card-body">
                                                            <select class="form-control" name="tipo_queja" id="tipo_queja"> 
                                                                <option value="0">--Seleccione el tipo de queja (*)--</option>
                                                                @foreach($tipoquejas as $tipo)
                                                                <option value="{{ $tipo->id_catalogo}}">{{ $tipo->descripcion}}</option>
                                                                @endforeach


                                                            </select>                                                            </div>                                         
            
                                                            <div class="card-body">
                                                                <textarea class="form-control" name="queja" id="queja" cols="30" rows="4" placeholder="Redacte su queja (*)"></textarea>
                                                            </div>
                                                        
                                                    
                                                            <div class="card-body">
                                                                <textarea class="form-control" name="proposicion" id="proposicion" cols="30" rows="4" placeholder="Que proponen para mejorar (*)"></textarea>
                                                            </div>
                                                    
                                                         
                                                </div>
                                                
                                                <div class="row">
                                                    <div class="button-row d-flex mt-4 col-12">
                                                        <button class="btn btn-primary js-btn-prev" type="button" title="Prev">Anterior</button>
                                                        <button class="btn btn-success ml-auto" type="submit" title="Send">Enviar</button>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


 <script  src="function.js"></script>
<script type="text/javascript">

$('div.alert').delay(5000).slideUp(300);

$('#departamento').on('change', function() 
	{
	  
	   		var id = $(this).val();   
            console.log(id);
			
			$.ajax
			({
				type : 'get',
				url : '{{URL::to('getMunicipios')}}',
				data:{'id':id},
				success:function(data)
				{
                    $("#municipio").empty();
                    console.log(data);
                    for (var i=0; i<data.length;i++)
                    {
                    
                    $("#municipio").append("<option value='" + data[i].id_catalogo+"'>"+data[i].descripcion+"</option>");
                                
                    }
                
				}
			});
			
   });


   $('#departamento_proveedor').on('change', function() 
	{  
	   		var id = $(this).val();   
            console.log(id);
			
			$.ajax
			({
				type : 'get',
				url : '{{URL::to('getMunicipios')}}',
				data:{'id':id},
				success:function(data)
				{
                    $("#municipio_proveedor").empty();
                    console.log(data);
                    for (var i=0; i<data.length;i++)
                    {
                    
                    $("#municipio_proveedor").append("<option value='" + data[i].id_catalogo+"'>"+data[i].descripcion+"</option>");
                                
                    }
                
				}
			});
			
   });


</script>
    
                   
@endsection



