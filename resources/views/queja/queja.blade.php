@extends('layouts.queja')

@section('content')
<div class="row justify-content-center m-3">
        <div class="col-md-8">
          <div class="card">
           <div class="card-header"><h2>Llene el siguiente campo para ver su queja</h2></div>
                <div class="card-body">
                  <form class="form-inline">
                    <div class="form-group mx-sm-5">
                      <label for="numero de queja" class="sr-only">Numero de queja</label>
                      <input type="text" class="form-control" id="numero_queja" placeholder="Ingrese el numero de su queja">
                    </div>
                    <button type="button" id="getQueja" class="btn btn-primary">Consultar</button>
                  </form>
                </div>
          </div>
        </div>

</div>
<div class="container col-md-12">
        <div class="panel panel-default pull-center" position="center">
                <table class="table" id="tabla_queja"> 
                    <thead>
                        <tr>
                            <th>Descripcion</th>
                            <th>Solicitud Consumidor</th>
                            <th>Fecha de Creacion</th>
                            <th>Negocio</th>                            
                            <th>Estado</th> 
                        </tr>
                    </thead>
                    <tbody>
                       
                    </tbody>
                </table>
        </div>
 </div>



<script type="text/javascript">
 $(document).ready(function() 
 {
      $('#tabla_queja').hide();
      $('div.alert').delay(5000).slideUp(300);
      $(document).on('click', '#getQueja', function(event)
      {		
          var numero_queja= $('#numero_queja').val();
          $("#tabla_queja tr").remove(); 
          	
          $.ajax({
                  type: "get",
                  url : '{{URL::to('getQueja')}}',			
                  data:{
                      'numero_queja':numero_queja},
                       success: function(response) {	
                       
                         console.log(response);
                          $('#tabla_queja').show();						
                              $('#tabla_queja')
                              .append('<tr><td>' + response[0].detalle_queja + '</td><td>' + response[0].solicitud_consumidor + '</td><td>' + response[0].fecha_creacion + '</td><td>'+response[0].proveedor_queja + '</td><td>'+response[0].estado_queja+'</td></tr>');
                      
                           }, 
                           error : function(xhr, status) {
                                alert('Numero de queja no encontrado');
                            },

                           
                });

       });


  });
</script>
@endsection