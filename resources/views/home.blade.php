@extends('layouts.app')
@section('title')
Resumen
  
@stop

@section('description')
    Datos estadisticos relacionados a las quejas
@stop

@section('breadcrumb')
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('home')}}">Inicio</a></li>
              <li class="breadcrumb-item active">Resumen</li>
            </ol>

@stop

@section('content')
      <div class="container-fluid mt-3">
        <!-- Info boxes -->
        <div class="row">
          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box">
              <span class="info-box-icon bg-info elevation-1"><i class="fas fa-rss-square"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Cantidad de quejas activas</span>
                <span class="info-box-number">
                  {{$quejas_activas}}
                  <!-- <small>%</small> -->
                </span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-info elevation-1"><i class="fas fa-cog"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Quejas en proceso</span>
                <span class="info-box-number">{{$quejas_proceso}}</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->

          <!-- fix for small devices only -->
          

         
        
          <!-- /.col -->
        </div>
        <!-- /.row -->

       
        
        


        <div class="card card-success">
              <div class="card-header">
                <h3 class="card-title">Quejas recibidas en los ultimos seis meses</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                </div>
              </div>
              <div class="card-body">
                <div class="chart">
                  <canvas id="stackedBarChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                </div>
              </div>
              <!-- /.card-body -->
            </div>
      
      </div><!--/. container-fluid -->
  
  <!-- /.content-wrapper -->
@endsection
@section('js')

<script>

$(function () {
  var datos=0;
  $.ajax
			({
				type : 'get',
        url : '{{URL::to('sixMonths')}}',
        async: false  ,
				success:function(data)
				{
          // console.log(data);
          datos = data;
                
				}
			});
      console.log(datos);
    var areaChartData = {
      labels  : ['Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
      datasets: [
        {
          label               : 'Quejas Recibidas',
          backgroundColor     : 'rgba(60,141,188,0.9)',
          borderColor         : 'rgba(60,141,188,0.8)',
          pointRadius          : false,
          pointColor          : '#3b8bba',
          pointStrokeColor    : 'rgba(60,141,188,1)',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(60,141,188,1)',
          data                : datos
        },
        
      ]
    }

       //---------------------
    //- STACKED BAR CHART -
    //---------------------
    var stackedBarChartCanvas = $('#stackedBarChart').get(0).getContext('2d')
    var stackedBarChartData = jQuery.extend(true, {}, areaChartData)
    var temp0 = areaChartData.datasets[0]
    // var temp1 = areaChartData.datasets[1]
    // stackedBarChartData.datasets[0] = temp1
    stackedBarChartData.datasets[1] = temp0

    var stackedBarChartOptions = {
      responsive              : true,
      maintainAspectRatio     : false,
      scales: {
        xAxes: [{
          stacked: true,
        }],
        yAxes: [{
          stacked: true
        }]
      }
    }

    var stackedBarChart = new Chart(stackedBarChartCanvas, {
      type: 'bar', 
      data: stackedBarChartData,
      options: stackedBarChartOptions
    })
  })

</script>


@endsection



