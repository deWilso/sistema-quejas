<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuejasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quejas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('numero_queja');
            $table->string('detalle_queja', 600);
            $table->string('solicitud_consumidor', 600);
            $table->date('fecha_creacion');
            $table->integer('estado_queja');
            $table->foreign('estado_queja')->references('id_catalogo')->on('catalogos');
            $table->integer('tipo_queja');
            $table->foreign('tipo_queja')->references('id_catalogo')->on('catalogos');
            $table->integer('sede_queja');
            $table->foreign('sede_queja')->references('id_catalogo')->on('catalogos');
            $table->unsignedInteger('persona_queja');
            $table->foreign('persona_queja')->references('id')->on('personas');
            $table->unsignedInteger('proveedor_queja');
            $table->foreign('proveedor_queja')->references('id')->on('proveedores');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quejas');
    }
}
