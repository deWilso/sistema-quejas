<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProveedoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proveedores', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('nit');
            $table->string('nombre', 100);
            $table->string('direccion', 100);
            $table->integer('departamento');
            $table->foreign('departamento')->references('id_catalogo')->on('catalogos');
            $table->integer('municipio');
            $table->foreign('municipio')->references('id_catalogo')->on('catalogos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proveedores');
    }
}
