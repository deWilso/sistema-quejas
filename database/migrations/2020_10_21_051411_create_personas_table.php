<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('primer_nombre',30);
            $table->string('segundo_nombre',30)->nullable();
            $table->string('primer_apellido',30);
            $table->string('segundo_apellido',30)->nullable();
            $table->string('apellido_casada',30)->nullable();
            $table->integer('departamento');
            $table->foreign('departamento')->references('id_catalogo')->on('catalogos');
            $table->integer('municipio');
            $table->foreign('municipio')->references('id_catalogo')->on('catalogos');
            $table->string('direccion',50)->nullable();
            $table->bigInteger('identificacion');
            $table->bigInteger('telefono');
            $table->integer('nacionalidad');
            $table->foreign('nacionalidad')->references('id_catalogo')->on('catalogos');
            $table->integer('genero');
            $table->foreign('genero')->references('id_catalogo')->on('catalogos');
            $table->integer('estado_civil');
            $table->foreign('estado_civil')->references('id_catalogo')->on('catalogos');
            $table->string('correo', 50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personas');
    }
}
