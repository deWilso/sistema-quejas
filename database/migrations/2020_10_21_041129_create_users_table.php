<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('primer_nombre',30);
            $table->string('segundo_nombre',30)->nullable();
            $table->string('primer_apellido',30);
            $table->string('segundo_apellido',30)->nullable();
            $table->string('apellido_casada',30)->nullable();
            $table->integer('departamento');
            $table->foreign('departamento')->references('id_catalogo')->on('catalogos');
            $table->integer('municipio');
            $table->foreign('municipio')->references('id_catalogo')->on('catalogos');
            $table->integer('sede');
            $table->foreign('sede')->references('id_catalogo')->on('catalogos');
            $table->string('direccion',50)->nullable();
            $table->bigInteger('identificacion')->nullable();
            $table->bigInteger('telefono')->unique();
            $table->date('fecha_nacimiento');
            $table->integer('genero');
            $table->foreign('genero')->references('id_catalogo')->on('catalogos');
            $table->integer('estado_civil');
            $table->foreign('estado_civil')->references('id_catalogo')->on('catalogos');
            $table->integer('rol');
            $table->foreign('rol')->references('id_catalogo')->on('catalogos');
            $table->string('email', 50)->unique();
            $table->timestamp('correo_verificado_por')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
