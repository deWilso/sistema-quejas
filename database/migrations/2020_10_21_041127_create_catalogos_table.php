<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCatalogosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('catalogos', function (Blueprint $table) {
            $table->integer('id_catalogo');
            $table->primary('id_catalogo');
            $table->string('descripcion', 60);
            $table->integer('id_catalogo_padre')->nullable();
            $table->foreign('id_catalogo_padre')->references('id_catalogo')->on('catalogos');
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('catalogos');
    }
}
