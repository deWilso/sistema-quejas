<?php

use Illuminate\Database\Seeder;

class Depto_Rol_CatalogoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Catalogos padre
        //Roles
        DB::table('catalogos')->insert([
            'id_catalogo' => 10,
            'descripcion' => 'Rol',
        ]);
        
         //Queja
         DB::table('catalogos')->insert([
            'id_catalogo'=>20,
            'descripcion' => 'Tipo de Queja',
        ]);

         //Estado Civil
         DB::table('catalogos')->insert([
            'id_catalogo'=>30,
            'descripcion' => 'Estado Civil',
        ]);
        //Genero

        DB::table('catalogos')->insert([
            'id_catalogo'=>40,
            'descripcion' => 'Genero',
        ]);
        //sedes de la diaco
        DB::table('catalogos')->insert([
            'id_catalogo'=>50,
            'descripcion' => 'Sede Diaco',
        ]);

        //nacionalidad
        DB::table('catalogos')->insert([
            'id_catalogo'=>60,
            'descripcion' => 'Nacionalidad',
        ]);
        //estados citas
        DB::table('catalogos')->insert([
            'id_catalogo'=>70,
            'descripcion' => 'Estado Cita',
        ]);

         //Departamentos
         DB::table('catalogos')->insert([
            'id_catalogo'=>100,
            'descripcion' => 'Departamento',
        ]);



//---------------------------------------------------------------------//
        //Fill Roles
 
        DB::table('catalogos')->insert([
            'id_catalogo' => 11,
            'descripcion' => 'Estandar',
            'id_catalogo_padre'=>10,
        ]);
        DB::table('catalogos')->insert([
            'id_catalogo' => 12,
            'descripcion' => 'Administrador',
            'id_catalogo_padre'=>10,
        ]);
//-----------------------------------------------------------------------//
        //Fill tipos de queja
        DB::table('catalogos')->insert([
            'id_catalogo' => 21,
            'descripcion' => 'Queja por Garantia',
            'id_catalogo_padre'=>20,
        ]);
        DB::table('catalogos')->insert([
            'id_catalogo' => 22,
            'descripcion' => 'Devolucion de Producto',
            'id_catalogo_padre'=>20,
        ]);

        DB::table('catalogos')->insert([
            'id_catalogo' => 23,
            'descripcion' => 'Queja por facturacion',
            'id_catalogo_padre'=>20,
        ]);
        DB::table('catalogos')->insert([
            'id_catalogo' => 24,
            'descripcion' => 'Servicio al cliente',
            'id_catalogo_padre'=>20,
        ]);


//------------------------------------------------------------------------//
        //fill estado civil
        DB::table('catalogos')->insert([
            'id_catalogo' => 31,
            'descripcion' => 'Soltero/a',
            'id_catalogo_padre'=>30,
        ]);
        DB::table('catalogos')->insert([
            'id_catalogo' => 32,
            'descripcion' => 'Casado/a',
            'id_catalogo_padre'=>30,
        ]);
      

//-----------------------------------------------------------------------//

          //fill Genero
          DB::table('catalogos')->insert([
            'id_catalogo' => 41,
            'descripcion' => 'Masculino',
            'id_catalogo_padre'=>40,
        ]);
        DB::table('catalogos')->insert([
            'id_catalogo' => 42,
            'descripcion' => 'Femenino',
            'id_catalogo_padre'=>40,
        ]);
        DB::table('catalogos')->insert([
            'id_catalogo' => 43,
            'descripcion' => 'Otro',
            'id_catalogo_padre'=>40,
        ]);
//----------------------------------------------------------------------//
        //fill sedes Diaco
        DB::table('catalogos')->insert([
            'id_catalogo' => 51,
            'descripcion' => 'Diaco Peten',
            'id_catalogo_padre'=>50,
        ]);
        DB::table('catalogos')->insert([
            'id_catalogo' => 52,
            'descripcion' => 'Diaco Alta Verapaz',
            'id_catalogo_padre'=>50,
        ]);
        DB::table('catalogos')->insert([
            'id_catalogo' => 53,
            'descripcion' => 'Baja Verapaz',
            'id_catalogo_padre'=>50,
        ]);
        DB::table('catalogos')->insert([
            'id_catalogo' => 54,
            'descripcion' => 'Diaco Ciudad de Guatemala',
            'id_catalogo_padre'=>50,
        ]);

    
        
//---------------------------------------------------------------//
            //fill nacionalidad
            DB::table('catalogos')->insert([
                'id_catalogo'=>61,
                'descripcion' => 'Nacional',
                'id_catalogo_padre'=>60,
            ]);
            DB::table('catalogos')->insert([
                'id_catalogo'=>62,
                'descripcion' => 'Extranjero',
                'id_catalogo_padre'=>60,
            ]);

//-------Fill estado cita-------------------------------------//
            DB::table('catalogos')->insert([
                'id_catalogo'=>71,
                'descripcion' => 'Activo',
                'id_catalogo_padre'=>70,

              ]);
              DB::table('catalogos')->insert([
                'id_catalogo'=>72,
                'descripcion' => 'En Proceso',
                'id_catalogo_padre'=>70,

              ]);
              DB::table('catalogos')->insert([
                'id_catalogo'=>73,
                'descripcion' => 'Finalizado',
                'id_catalogo_padre'=>70,

              ]);
//------------------------------------------------------------------//
        //Fill departamentos
      
        DB::table('catalogos')->insert([
            'id_catalogo'=>1000,
            'descripcion' => 'Guatemala',
            'id_catalogo_padre'=>100,
        ]);
        DB::table('catalogos')->insert([
            'id_catalogo'=>2000,
            'descripcion' => 'El Progreso',
            'id_catalogo_padre'=>100,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>3000,
            'descripcion' => 'Sacatapequez',
            'id_catalogo_padre'=>100,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>4000,
            'descripcion' => 'Chimaltenango',
            'id_catalogo_padre'=>100,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>5000,
            'descripcion' => 'Escuintla',
            'id_catalogo_padre'=>100,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>6000,
            'descripcion' => 'Santa Rosa',
            'id_catalogo_padre'=>100,
        ]); 

        DB::table('catalogos')->insert([
            'id_catalogo'=>7000,
            'descripcion' => 'Sololá',
            'id_catalogo_padre'=>100,
        ]); 

        DB::table('catalogos')->insert([
            'id_catalogo'=> 8000,
            'descripcion' => 'Totonicapán',
            'id_catalogo_padre'=>100,
        ]); 

        DB::table('catalogos')->insert([
            'id_catalogo'=>9000,
            'descripcion' => 'Quetzaltenango',
            'id_catalogo_padre'=>100,
        ]); 

        DB::table('catalogos')->insert([
            'id_catalogo'=>10000,
            'descripcion' => 'Suchitepequez',
            'id_catalogo_padre'=>100,
        ]); 

        DB::table('catalogos')->insert([
            'id_catalogo'=>11000,
            'descripcion' => 'Retalhuleu',
            'id_catalogo_padre'=>100,
        ]); 

        DB::table('catalogos')->insert([
            'id_catalogo'=>22000,
            'descripcion' => 'Jutiapa',
            'id_catalogo_padre'=>100,
        ]); 

        DB::table('catalogos')->insert([
            'id_catalogo'=>12000,
            'descripcion' => 'San Marcos',
            'id_catalogo_padre'=>100,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>13000,
            'descripcion' => 'Huehuetenango',
            'id_catalogo_padre'=>100,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>14000,
            'descripcion' => 'Quiché',
            'id_catalogo_padre'=>100,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>15000,
            'descripcion' => 'Baja Verapáz',
            'id_catalogo_padre'=>100,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>16000,
            'descripcion' => 'Alta Verapáz',
            'id_catalogo_padre'=>100,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>17000,
            'descripcion' => 'Petén',
            'id_catalogo_padre'=>100,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>18000,
            'descripcion' => 'Izabal',
            'id_catalogo_padre'=>100,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>19000,
            'descripcion' => 'Zacapa',
            'id_catalogo_padre'=>100,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>20000,
            'descripcion' => 'Chiquimula',
            'id_catalogo_padre'=>100,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>21000,
            'descripcion' => 'Jalapa',
            'id_catalogo_padre'=>100,
        ]); 
        
    }
}
