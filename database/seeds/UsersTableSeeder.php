<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'primer_nombre' => 'Cornelio',
            'segundo_nombre'=> 'de Wilso',
            'primer_apellido'=> 'Leal',
            'segundo_apellido'=>'Tut',
            'departamento'=>1000,
            'municipio'=> 1051,
            'sede'=>53,
            'direccion' => '5ta. Calle 5-63, Zona 6, Barrio San Vicente',
            'identificacion' => 2250303571708,
            'telefono'=>30727541,
            'fecha_nacimiento'=>'1992-10-12',
            'genero'=>41,
            'estado_civil'=>31,
            'rol'=>12,
            'email'=>'admin@gmail.com',
            'password'=>bcrypt('1234'),
            

        ]); 
        DB::table('users')->insert([
            'primer_nombre' => 'Carlos ',
            'primer_apellido'=> 'Rodriguez',
            'segundo_apellido'=>'Macal',
            'departamento'=>17000,
            'municipio'=> 17003,
            'sede'=>51,
            'direccion' => '1ra. Calle 12-15 Barrio Las Flores',
            'identificacion' => 1234303561802,
            'telefono'=>33456898,
            'fecha_nacimiento'=>'1992-04-12',
            'genero'=>41,
            'estado_civil'=>31,
            'rol'=>11,
            'email'=>'estandar@gmail.com',
            'password'=>bcrypt('1234'),
            

        ]); 
    }
}
