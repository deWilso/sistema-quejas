<?php

use Illuminate\Database\Seeder;

class Municipios_CatalogoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Municipios departamento Guatemala
        DB::table('catalogos')->insert([
            'id_catalogo'=>1050,
            'descripcion' => 'Guatemala',
            'id_catalogo_padre'=>1000,
        ]);
        DB::table('catalogos')->insert([
            'id_catalogo'=>1051,
            'descripcion' => 'Santa Catarina Pínula',
            'id_catalogo_padre'=>1000,
        ]);
        DB::table('catalogos')->insert([
            'id_catalogo'=>1052,
            'descripcion' => 'San José Pínula',
            'id_catalogo_padre'=>1000,
        ]);
        DB::table('catalogos')->insert([
            'id_catalogo'=>1053,
            'descripcion' => 'San José Del Golfo',
            'id_catalogo_padre'=>1000,
        ]);
        DB::table('catalogos')->insert([
            'id_catalogo'=>1054,
            'descripcion' => 'Palencia',
            'id_catalogo_padre'=>1000,
        ]);
        DB::table('catalogos')->insert([
            'id_catalogo'=>1055,
            'descripcion' => 'Chinautla',
            'id_catalogo_padre'=>1000,
        ]);
        DB::table('catalogos')->insert([
            'id_catalogo'=>1056,
            'descripcion' => 'San Pedro Ayampuc',
            'id_catalogo_padre'=>1000,
        ]);
        DB::table('catalogos')->insert([
            'id_catalogo'=>1057,
            'descripcion' => 'Mixco',
            'id_catalogo_padre'=>1000,
        ]);
        DB::table('catalogos')->insert([
            'id_catalogo'=>1058,
            'descripcion' => 'San Pedro Sacatepéquez',
            'id_catalogo_padre'=>1000,
        ]);
        DB::table('catalogos')->insert([
            'id_catalogo'=>1059,
            'descripcion' => 'San Juan Sacatepéquez',
            'id_catalogo_padre'=>1000,
        ]);
        DB::table('catalogos')->insert([
            'id_catalogo'=>1060,
            'descripcion' => 'San Raymundo',
            'id_catalogo_padre'=>1000,
        ]);
        DB::table('catalogos')->insert([
            'id_catalogo'=>1061,
            'descripcion' => 'Chuarrancho',
            'id_catalogo_padre'=>1000,
        ]);
        DB::table('catalogos')->insert([
            'id_catalogo'=>1062,
            'descripcion' => 'Fraijanes',
            'id_catalogo_padre'=>1000,
        ]);
        DB::table('catalogos')->insert([
            'id_catalogo'=>1063,
            'descripcion' => 'Amatitlán',
            'id_catalogo_padre'=>1000,
        ]);
        DB::table('catalogos')->insert([
            'id_catalogo'=>1064,
            'descripcion' => 'Villa Nueva',
            'id_catalogo_padre'=>1000,
        ]);
        DB::table('catalogos')->insert([
            'id_catalogo'=>1065,
            'descripcion' => 'Villa Canales',
            'id_catalogo_padre'=>1000,
        ]);
        DB::table('catalogos')->insert([
            'id_catalogo'=>1066,
            'descripcion' => 'Petapa',
            'id_catalogo_padre'=>1000,
        ]);

        //Municipios de el Progreso
        
        DB::table('catalogos')->insert([
            'id_catalogo'=>2001,
            'descripcion' => 'Guastatoya',
            'id_catalogo_padre'=>2000,
        ]);
        DB::table('catalogos')->insert([
            'id_catalogo'=>2002,
            'descripcion' => 'Morazán',
            'id_catalogo_padre'=>2000,
        ]);
        DB::table('catalogos')->insert([
            'id_catalogo'=>2003,
            'descripcion' => 'San Agustin Acasaguastlán',
            'id_catalogo_padre'=>2000,
        ]);
        DB::table('catalogos')->insert([
            'id_catalogo'=>2004,
            'descripcion' => 'San Cristóbal Acasaguastlán',
            'id_catalogo_padre'=>2000,
        ]);
        DB::table('catalogos')->insert([
            'id_catalogo'=>2005,
            'descripcion' => 'El Jicaro',
            'id_catalogo_padre'=>2000,
        ]);

        DB::table('catalogos')->insert([
            'id_catalogo'=>2006,
            'descripcion' => 'Sansare',
            'id_catalogo_padre'=>2000,
        ]);

        DB::table('catalogos')->insert([
            'id_catalogo'=>2007,
            'descripcion' => 'Sanarate',
            'id_catalogo_padre'=>2000,
        ]);
        DB::table('catalogos')->insert([
            'id_catalogo'=>2008,
            'descripcion' => 'San Antonio La Paz',
            'id_catalogo_padre'=>2000,
        ]);

        //Municipios de Saquetepequez

        DB::table('catalogos')->insert([
            'id_catalogo'=>3001,
            'descripcion' => 'Antigua Guatemala',
            'id_catalogo_padre'=>3000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>3002,
            'descripcion' => 'Jocotenango',
            'id_catalogo_padre'=>3000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>3003,
            'descripcion' => 'Pastores',
            'id_catalogo_padre'=>3000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>3004,
            'descripcion' => 'Sumpango',
            'id_catalogo_padre'=>3000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>3005,
            'descripcion' => 'Santo Domingo Xenacoj',
            'id_catalogo_padre'=>3000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>3006,
            'descripcion' => 'Santiago Sacatepéquez',
            'id_catalogo_padre'=>3000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>3007,
            'descripcion' => 'San Bartolomé Milpas Altas',
            'id_catalogo_padre'=>3000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>3008,
            'descripcion' => 'San Lucas Sacatepequez',
            'id_catalogo_padre'=>3000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>3009,
            'descripcion' => 'Santa Lucia Milpas Altas',
            'id_catalogo_padre'=>3000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>3010,
            'descripcion' => 'Magdalena Milpas Altas',
            'id_catalogo_padre'=>3000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>3011,
            'descripcion' => 'Santa María De Jesús',
            'id_catalogo_padre'=>3000,
        ]); 

        DB::table('catalogos')->insert([
            'id_catalogo'=>3012,
            'descripcion' => 'Ciudad Vieja',
            'id_catalogo_padre'=>3000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>3013,
            'descripcion' => 'San Miguel Dueñas',
            'id_catalogo_padre'=>3000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>3014,
            'descripcion' => 'Alotenango',
            'id_catalogo_padre'=>3000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>3015,
            'descripcion' => 'San Antonio Aguas Calientes',
            'id_catalogo_padre'=>3000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>3016,
            'descripcion' => 'Santa Catarina Barahona',
            'id_catalogo_padre'=>3000,
        ]); 
       

        //Municipios de chimaltenango

        DB::table('catalogos')->insert([
            'id_catalogo'=>4001,
            'descripcion' => 'Chimaltenango',
            'id_catalogo_padre'=>4000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>4002,
            'descripcion' => 'San José Poaquíl',
            'id_catalogo_padre'=>4000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>4003,
            'descripcion' => 'San Martín Jilotepeque',
            'id_catalogo_padre'=>4000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>4004,
            'descripcion' => 'San Juan Comalapa',
            'id_catalogo_padre'=>4000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>4005,
            'descripcion' => 'Santa Apolonia',
            'id_catalogo_padre'=>4000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>4006,
            'descripcion' => 'Tecpán Guatemala',
            'id_catalogo_padre'=>4000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>4007,
            'descripcion' => 'Patzún',
            'id_catalogo_padre'=>4000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>4008,
            'descripcion' => 'Pochuta',
            'id_catalogo_padre'=>4000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>4009,
            'descripcion' => 'Patzicía',
            'id_catalogo_padre'=>4000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>4010,
            'descripcion' => 'Santa Cruz Balanyá',
            'id_catalogo_padre'=>4000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>4011,
            'descripcion' => 'Acatenango',
            'id_catalogo_padre'=>4000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>4012,
            'descripcion' => 'San Pedro Yepocapa',
            'id_catalogo_padre'=>4000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>4013,
            'descripcion' => 'San Andrés Itzapa',
            'id_catalogo_padre'=>4000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>4014,
            'descripcion' => 'Parramos',
            'id_catalogo_padre'=>4000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>4015,
            'descripcion' => 'Zaragoza',
            'id_catalogo_padre'=>4000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>4016,
            'descripcion' => 'El Tejar',
            'id_catalogo_padre'=>4000,
        ]); 

        //Municipios de Escuintla

        DB::table('catalogos')->insert([
            'id_catalogo'=>5001,
            'descripcion' => 'Escuintla',
            'id_catalogo_padre'=>5000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>5002,
            'descripcion' => 'Guanagazapa',
            'id_catalogo_padre'=>5000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>5003,
            'descripcion' => 'Iztapa',
            'id_catalogo_padre'=>5000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>5004,
            'descripcion' => 'La Democracia',
            'id_catalogo_padre'=>5000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>5005,
            'descripcion' => 'La Gomera',
            'id_catalogo_padre'=>5000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>5006,
            'descripcion' => 'Masagua',
            'id_catalogo_padre'=>5000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>5007,
            'descripcion' => 'Nueva Concepción',
            'id_catalogo_padre'=>5000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>5008,
            'descripcion' => 'Palín',
            'id_catalogo_padre'=>5000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>5009,
            'descripcion' => 'San José',
            'id_catalogo_padre'=>5000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>5010,
            'descripcion' => 'San Vicente Pacaya',
            'id_catalogo_padre'=>5000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>5011,
            'descripcion' => 'Santa Lucía Cotzumalguapa',
            'id_catalogo_padre'=>5000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>5012,
            'descripcion' => 'Sipacate',
            'id_catalogo_padre'=>5000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>5013,
            'descripcion' => 'Siquinalá',
            'id_catalogo_padre'=>5000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>5014,
            'descripcion' => 'Tiquisate',
            'id_catalogo_padre'=>5000,
        ]); 


        // Municipios de Santa Rosa
        DB::table('catalogos')->insert([
            'id_catalogo'=>6001,
            'descripcion' => 'Cuilapa',
            'id_catalogo_padre'=>6000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>6002,
            'descripcion' => 'Casillas',
            'id_catalogo_padre'=>6000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>6003,
            'descripcion' => 'Chiquimulilla',
            'id_catalogo_padre'=>6000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>6004,
            'descripcion' => 'Guazacapán',
            'id_catalogo_padre'=>6000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>6005,
            'descripcion' => 'Nueva Santa Rosa',
            'id_catalogo_padre'=>6000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>6006,
            'descripcion' => 'Oratorio',
            'id_catalogo_padre'=>6000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>6007,
            'descripcion' => 'Pueblo Nuevo Viñas',
            'id_catalogo_padre'=>6000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>6008,
            'descripcion' => 'San Juan Tecuaco',
            'id_catalogo_padre'=>6000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>6009,
            'descripcion' => 'San Rafaél Las Flores',
            'id_catalogo_padre'=>6000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>6010,
            'descripcion' => 'Santa Cruz Naranjo',
            'id_catalogo_padre'=>6000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>6011,
            'descripcion' => 'Santa María Ixhuatán',
            'id_catalogo_padre'=>6000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>6012,
            'descripcion' => 'Santa Rosa de Lima',
            'id_catalogo_padre'=>6000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>6013,
            'descripcion' => 'Taxisco',
            'id_catalogo_padre'=>6000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>6014,
            'descripcion' => 'Barberena',
            'id_catalogo_padre'=>6000,
        ]); 
        

        //Municipios de Solola
        DB::table('catalogos')->insert([
            'id_catalogo'=>7001,
            'descripcion' => 'Sololá',
            'id_catalogo_padre'=>7000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>7002,
            'descripcion' => 'Concepción',
            'id_catalogo_padre'=>7000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>7003,
            'descripcion' => 'Nahualá',
            'id_catalogo_padre'=>7000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>7004,
            'descripcion' => 'Panajachel',
            'id_catalogo_padre'=>7000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>7005,
            'descripcion' => 'San Andrés Semetabaj',
            'id_catalogo_padre'=>7000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>7006,
            'descripcion' => 'San Antonio Palopó',
            'id_catalogo_padre'=>7000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>7007,
            'descripcion' => 'San José Chacayá',
            'id_catalogo_padre'=>7000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>7008,
            'descripcion' => 'San Juan La Laguna',
            'id_catalogo_padre'=>7000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>7009,
            'descripcion' => 'San Lucas Tolimán',
            'id_catalogo_padre'=>7000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>7010,
            'descripcion' => 'San Marcos La Laguna',
            'id_catalogo_padre'=>7000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>7011,
            'descripcion' => 'San Pablo La Laguna',
            'id_catalogo_padre'=>7000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>7012,
            'descripcion' => 'San Pedro La Laguna',
            'id_catalogo_padre'=>7000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>7013,
            'descripcion' => 'Santa Catarina Ixtahuacan',
            'id_catalogo_padre'=>7000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>7014,
            'descripcion' => 'Santa Catarina Palopó',
            'id_catalogo_padre'=>7000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>7015,
            'descripcion' => 'Santa Clara La Laguna',
            'id_catalogo_padre'=>7000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>7016,
            'descripcion' => 'Santa Cruz La Laguna',
            'id_catalogo_padre'=>7000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>7017,
            'descripcion' => 'Santa Lucía Utatlán',
            'id_catalogo_padre'=>7000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>7018,
            'descripcion' => 'Santa María Visitación',
            'id_catalogo_padre'=>7000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>7019,
            'descripcion' => 'Santiago Atitlán',
            'id_catalogo_padre'=>7000,
        ]); 


        //Municipios de totonicapan
        DB::table('catalogos')->insert([
            'id_catalogo'=> 8001,
            'descripcion' => 'Totonicapán',
            'id_catalogo_padre'=>8000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=> 8002,
            'descripcion' => 'Momostenango',
            'id_catalogo_padre'=>8000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=> 8003,
            'descripcion' => 'San Andrés Xecul',
            'id_catalogo_padre'=>8000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=> 8004,
            'descripcion' => 'San Bartolo',
            'id_catalogo_padre'=>8000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=> 8005,
            'descripcion' => 'San Cristóbal Totonicapán',
            'id_catalogo_padre'=>8000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=> 8006,
            'descripcion' => 'San Francisco El Alto',
            'id_catalogo_padre'=>8000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=> 8007,
            'descripcion' => 'Santa Lucía La Reforma',
            'id_catalogo_padre'=>8000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=> 8008,
            'descripcion' => 'Santa María Chiquimula',
            'id_catalogo_padre'=>8000,
        ]); 

        // Municipios de quetzaltenango

        DB::table('catalogos')->insert([
            'id_catalogo'=>9001,
            'descripcion' => 'Almolonga',
            'id_catalogo_padre'=>9000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>9002,
            'descripcion' => 'Cabricán',
            'id_catalogo_padre'=>9000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>9003,
            'descripcion' => 'Cajolá',
            'id_catalogo_padre'=>9000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>9004,
            'descripcion' => 'Cantel',
            'id_catalogo_padre'=>9000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>9005,
            'descripcion' => 'Coatepeque',
            'id_catalogo_padre'=>9000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>9006,
            'descripcion' => 'Colomba',
            'id_catalogo_padre'=>9000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>9007,
            'descripcion' => 'Concepción Chiquirichapa',
            'id_catalogo_padre'=>9000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>9008,
            'descripcion' => 'El Palmar',
            'id_catalogo_padre'=>9000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>9009,
            'descripcion' => 'Flores Costa Cuca',
            'id_catalogo_padre'=>9000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>9010,
            'descripcion' => 'Génova',
            'id_catalogo_padre'=>9000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>9011,
            'descripcion' => 'Huitán',
            'id_catalogo_padre'=>9000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>9012,
            'descripcion' => 'La Esperanza',
            'id_catalogo_padre'=>9000,
        ]); 

        DB::table('catalogos')->insert([
            'id_catalogo'=>9013,
            'descripcion' => 'Olintepeque',
            'id_catalogo_padre'=>9000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>9014,
            'descripcion' => 'San Juan Ostuncalco',
            'id_catalogo_padre'=>9000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>9015,
            'descripcion' => 'Palestina de Los Altos',
            'id_catalogo_padre'=>9000,
        ]); 
        
        DB::table('catalogos')->insert([
            'id_catalogo'=>9016,
            'descripcion' => 'Quetzaltenango',
            'id_catalogo_padre'=>9000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>9017,
            'descripcion' => 'Salcajá',
            'id_catalogo_padre'=>9000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>9018,
            'descripcion' => 'San Carlos Sija',
            'id_catalogo_padre'=>9000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>9019,
            'descripcion' => 'San Francisco La Unión',
            'id_catalogo_padre'=>9000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>9020,
            'descripcion' => 'San Martín Sacatepéquez',
            'id_catalogo_padre'=>9000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>9021,
            'descripcion' => 'San Mateo',
            'id_catalogo_padre'=>9000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>9022,
            'descripcion' => 'San Miguel Sigüilá',
            'id_catalogo_padre'=>9000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>9023,
            'descripcion' => 'Sibilia',
            'id_catalogo_padre'=>9000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>9024,
            'descripcion' => 'Zunil',
            'id_catalogo_padre'=>9000,
        ]); 

        //Municipios de Suchitepequez
        DB::table('catalogos')->insert([
            'id_catalogo'=>10001,
            'descripcion' => 'Mazatenango',
            'id_catalogo_padre'=>10000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>10002,
            'descripcion' => 'Chicacao',
            'id_catalogo_padre'=>10000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>10003,
            'descripcion' => 'Cuyotenango',
            'id_catalogo_padre'=>10000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>10004,
            'descripcion' => 'Patulul',
            'id_catalogo_padre'=>10000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>10005,
            'descripcion' => 'Pueblo Nuevo',
            'id_catalogo_padre'=>10000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>10006,
            'descripcion' => 'Río Bravo',
            'id_catalogo_padre'=>10000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>10007,
            'descripcion' => 'Samayac',
            'id_catalogo_padre'=>10000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>10008,
            'descripcion' => 'San Antonio Suchitepéquez',
            'id_catalogo_padre'=>10000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>10009,
            'descripcion' => 'San Bernardino',
            'id_catalogo_padre'=>10000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>10010,
            'descripcion' => 'San José El Ídolo',
            'id_catalogo_padre'=>10000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>10011,
            'descripcion' => 'San Francisco Zapotitlán',
            'id_catalogo_padre'=>10000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>10012,
            'descripcion' => 'San Gabriel',
            'id_catalogo_padre'=>10000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>10013,
            'descripcion' => 'San Juan Bautista',
            'id_catalogo_padre'=>10000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>10014,
            'descripcion' => 'San Lorenzo',
            'id_catalogo_padre'=>10000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>10015,
            'descripcion' => 'San Miguel Panán',
            'id_catalogo_padre'=>10000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>10016,
            'descripcion' => 'San Pablo Jocopilas',
            'id_catalogo_padre'=>10000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>10017,
            'descripcion' => 'Santa Barbara',
            'id_catalogo_padre'=>10000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>10018,
            'descripcion' => 'Santo Domingo Suchitepequez',
            'id_catalogo_padre'=>10000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>10019,
            'descripcion' => 'Santo Tomas La Unión',
            'id_catalogo_padre'=>10000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>10020,
            'descripcion' => 'Zunilito',
            'id_catalogo_padre'=>10000,
        ]); 

        //Municipios de Retalhuleu

        DB::table('catalogos')->insert([
            'id_catalogo'=>11001,
            'descripcion' => 'Champerico',
            'id_catalogo_padre'=>11000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>11002,
            'descripcion' => 'El Asintal',
            'id_catalogo_padre'=>11000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>11003,
            'descripcion' => 'Nuevo San Carlos',
            'id_catalogo_padre'=>11000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>11004,
            'descripcion' => 'Retalhuleu',
            'id_catalogo_padre'=>11000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>11005,
            'descripcion' => 'San Andrés Villa Seca',
            'id_catalogo_padre'=>11000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>11006,
            'descripcion' => 'San Martín Zapotitlán',
            'id_catalogo_padre'=>11000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>11007,
            'descripcion' => 'San Felipe',
            'id_catalogo_padre'=>11000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>11008,
            'descripcion' => 'San Sebastián',
            'id_catalogo_padre'=>11000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>11009,
            'descripcion' => 'Santa Cruz Muluá',
            'id_catalogo_padre'=>11000,
        ]); 

        //Municipios de Jutiapa
        DB::table('catalogos')->insert([
            'id_catalogo'=>22001,
            'descripcion' => 'Jutiapa',
            'id_catalogo_padre'=>22000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>22002,
            'descripcion' => 'Agua Blanca',
            'id_catalogo_padre'=>22000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>22003,
            'descripcion' => 'Asunción Mita',
            'id_catalogo_padre'=>22000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>22004,
            'descripcion' => 'Atescatempa',
            'id_catalogo_padre'=>22000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>22005,
            'descripcion' => 'Comapa',
            'id_catalogo_padre'=>22000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>22006,
            'descripcion' => 'Conguaco',
            'id_catalogo_padre'=>22000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>22007,
            'descripcion' => 'El Adelanto',
            'id_catalogo_padre'=>22000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>22008,
            'descripcion' => 'El Progreso',
            'id_catalogo_padre'=>22000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>22009,
            'descripcion' => 'Jalpatagua',
            'id_catalogo_padre'=>22000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>22010,
            'descripcion' => 'Jeréz',
            'id_catalogo_padre'=>22000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>22011,
            'descripcion' => 'Moyuta',
            'id_catalogo_padre'=>22000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>22012,
            'descripcion' => 'Pasaco',
            'id_catalogo_padre'=>22000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>22013,
            'descripcion' => 'Quesada',
            'id_catalogo_padre'=>22000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>22014,
            'descripcion' => 'San José Acatempa',
            'id_catalogo_padre'=>22000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>22015,
            'descripcion' => 'Santa Catarina Mita',
            'id_catalogo_padre'=>22000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>22016,
            'descripcion' => 'Yupiltepeque',
            'id_catalogo_padre'=>22000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>22017,
            'descripcion' => 'Zapotitlán',
            'id_catalogo_padre'=>22000,
        ]); 

        //Municipios de San Marcos

        DB::table('catalogos')->insert([
            'id_catalogo'=>12001,
            'descripcion' => 'San Marcos',
            'id_catalogo_padre'=>12000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>12002,
            'descripcion' => 'Ayutla',
            'id_catalogo_padre'=>12000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>12003,
            'descripcion' => 'Catarina',
            'id_catalogo_padre'=>12000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>12004,
            'descripcion' => 'Comitancillo',
            'id_catalogo_padre'=>12000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>12005,
            'descripcion' => 'Concepción Tutuapa',
            'id_catalogo_padre'=>12000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>12006,
            'descripcion' => 'El Quetzal',
            'id_catalogo_padre'=>12000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>12007,
            'descripcion' => 'El Rodeo',
            'id_catalogo_padre'=>12000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>12008,
            'descripcion' => 'El Tumbador',
            'id_catalogo_padre'=>12000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>12009,
            'descripcion' => 'Ixchiguán',
            'id_catalogo_padre'=>12000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>12010,
            'descripcion' => 'La Reforma',
            'id_catalogo_padre'=>12000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>12011,
            'descripcion' => 'Malacatán',
            'id_catalogo_padre'=>12000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>12012,
            'descripcion' => 'Nuevo Progreso',
            'id_catalogo_padre'=>12000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>12013,
            'descripcion' => 'Ocós',
            'id_catalogo_padre'=>12000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>12014,
            'descripcion' => 'Pajapita',
            'id_catalogo_padre'=>12000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>12015,
            'descripcion' => 'Esquipulas Palo Gordo',
            'id_catalogo_padre'=>12000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>12016,
            'descripcion' => 'San Antonio Sacatepéquez',
            'id_catalogo_padre'=>12000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>12017,
            'descripcion' => 'San Cristóbal Cucho',
            'id_catalogo_padre'=>12000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>12018,
            'descripcion' => 'San José Ojetenam',
            'id_catalogo_padre'=>12000,
        ]); 

        DB::table('catalogos')->insert([
            'id_catalogo'=>12019,
            'descripcion' => 'San Lorenzo',
            'id_catalogo_padre'=>12000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>12020,
            'descripcion' => 'San Miguel Ixtahuacán',
            'id_catalogo_padre'=>12000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>12021,
            'descripcion' => 'San Pablo',
            'id_catalogo_padre'=>12000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>12022,
            'descripcion' => 'San Pedro Sacatepéquez',
            'id_catalogo_padre'=>12000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>12023,
            'descripcion' => 'San Rafaél Pie de La Cuesta',
            'id_catalogo_padre'=>12000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>12024,
            'descripcion' => 'Sibinal',
            'id_catalogo_padre'=>12000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>12025,
            'descripcion' => 'Sipacapa',
            'id_catalogo_padre'=>12000,
        ]); 

        DB::table('catalogos')->insert([
            'id_catalogo'=>12026,
            'descripcion' => 'Tacaná',
            'id_catalogo_padre'=>12000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>12027,
            'descripcion' => 'Tajumulco',
            'id_catalogo_padre'=>12000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>12028,
            'descripcion' => 'Tejutla',
            'id_catalogo_padre'=>12000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>12029,
            'descripcion' => 'Río Blanco',
            'id_catalogo_padre'=>12000,
        ]); 


        // Municipios de Huehuetenango
        DB::table('catalogos')->insert([
            'id_catalogo'=>13001,
            'descripcion' => 'Aguacatán',
            'id_catalogo_padre'=>13000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>13002,
            'descripcion' => 'Chiantla',
            'id_catalogo_padre'=>13000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>13003,
            'descripcion' => 'Colotenango',
            'id_catalogo_padre'=>13000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>13004,
            'descripcion' => 'Concepción Huista',
            'id_catalogo_padre'=>13000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>13005,
            'descripcion' => 'Cuilco',
            'id_catalogo_padre'=>13000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>13006,
            'descripcion' => 'Huehuetenango',
            'id_catalogo_padre'=>13000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>13007,
            'descripcion' => 'Jacaltenango',
            'id_catalogo_padre'=>13000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>13008,
            'descripcion' => 'La Democracia',
            'id_catalogo_padre'=>13000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>13009,
            'descripcion' => 'La Libertad',
            'id_catalogo_padre'=>13000,
        ]); 

        DB::table('catalogos')->insert([
            'id_catalogo'=>13010,
            'descripcion' => 'Malacatancito',
            'id_catalogo_padre'=>13000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>13011,
            'descripcion' => 'Nentón',
            'id_catalogo_padre'=>13000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>13012,
            'descripcion' => 'San Antonio Huista',
            'id_catalogo_padre'=>13000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>13013,
            'descripcion' => 'San Gaspar Ixchil',
            'id_catalogo_padre'=>13000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>13014,
            'descripcion' => 'San Ildefonso Ixtahuacán',
            'id_catalogo_padre'=>13000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>13015,
            'descripcion' => 'San Juan Atitán',
            'id_catalogo_padre'=>13000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>13016,
            'descripcion' => 'San Juan Ixcoy',
            'id_catalogo_padre'=>13000,
        ]); 
        
        DB::table('catalogos')->insert([
            'id_catalogo'=>13017,
            'descripcion' => 'San Mateo Ixtatán',
            'id_catalogo_padre'=>13000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>13018,
            'descripcion' => 'San Miguel Acatán',
            'id_catalogo_padre'=>13000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>13019,
            'descripcion' => 'San Pedro Necta',
            'id_catalogo_padre'=>13000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>13020,
            'descripcion' => 'San Pedro Soloma',
            'id_catalogo_padre'=>13000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>13021,
            'descripcion' => 'Aguacatán',
            'id_catalogo_padre'=>13000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>13022,
            'descripcion' => 'San Rafael La Independencia',
            'id_catalogo_padre'=>13000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>13023,
            'descripcion' => 'San Rafael Petzal',
            'id_catalogo_padre'=>13000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>13024,
            'descripcion' => 'San Sebastián Coatán',
            'id_catalogo_padre'=>13000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>13025,
            'descripcion' => 'San Sebastián Huehuetenango',
            'id_catalogo_padre'=>13000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>13026,
            'descripcion' => 'Santa Ana Huista',
            'id_catalogo_padre'=>13000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>13027,
            'descripcion' => 'Santa Bárbara',
            'id_catalogo_padre'=>13000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>13028,
            'descripcion' => 'Santa Cruz Barillas',
            'id_catalogo_padre'=>13000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>13029,
            'descripcion' => 'Santa Eulalia',
            'id_catalogo_padre'=>13000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>13030,
            'descripcion' => 'Santiago Chimaltenango',
            'id_catalogo_padre'=>13000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>13031,
            'descripcion' => 'Tectitán',
            'id_catalogo_padre'=>13000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>13032,
            'descripcion' => 'Todos Santos Cuchumatán',
            'id_catalogo_padre'=>13000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>13033,
            'descripcion' => 'Unión Cantinil',
            'id_catalogo_padre'=>13000,
        ]); 
        
        //Municipios de quiche
        DB::table('catalogos')->insert([
            'id_catalogo'=>14001,
            'descripcion' => 'Santa Cruz del Quiché',
            'id_catalogo_padre'=>14000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>14002,
            'descripcion' => 'Canillá',
            'id_catalogo_padre'=>14000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>14003,
            'descripcion' => 'Chajul',
            'id_catalogo_padre'=>14000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>14004,
            'descripcion' => 'Chicamán',
            'id_catalogo_padre'=>14000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>14005,
            'descripcion' => 'Chiché',
            'id_catalogo_padre'=>14000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>14006,
            'descripcion' => 'Chichicastenango',
            'id_catalogo_padre'=>14000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>14007,
            'descripcion' => 'Chinique',
            'id_catalogo_padre'=>14000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>14008,
            'descripcion' => 'Cunén',
            'id_catalogo_padre'=>14000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>14009,
            'descripcion' => 'Ixcán',
            'id_catalogo_padre'=>14000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>14010,
            'descripcion' => 'Joyabaj',
            'id_catalogo_padre'=>14000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>14011,
            'descripcion' => 'Nebaj',
            'id_catalogo_padre'=>14000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>14012,
            'descripcion' => 'Pachalum',
            'id_catalogo_padre'=>14000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>14013,
            'descripcion' => 'Patzité',
            'id_catalogo_padre'=>14000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>14014,
            'descripcion' => 'Sacapulas',
            'id_catalogo_padre'=>14000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>14015,
            'descripcion' => 'San Andrés Sajcabajá',
            'id_catalogo_padre'=>14000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>14016,
            'descripcion' => 'San Antonio Ilotenango',
            'id_catalogo_padre'=>14000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>14017,
            'descripcion' => 'San Bartolomé Jocotenango',
            'id_catalogo_padre'=>14000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>14018,
            'descripcion' => 'San Juan Cotzal',
            'id_catalogo_padre'=>14000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>14019,
            'descripcion' => 'San Pedro Jocopilas',
            'id_catalogo_padre'=>14000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>14020,
            'descripcion' => 'Uspantán',
            'id_catalogo_padre'=>14000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>14021,
            'descripcion' => 'Zacualpa',
            'id_catalogo_padre'=>14000,
        ]); 

        //Munipios Baja Verapaz
        DB::table('catalogos')->insert([
            'id_catalogo'=>15001,
            'descripcion' => 'Cubulco',
            'id_catalogo_padre'=>15000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>15002,
            'descripcion' => 'Santa Cruz el Chol',
            'id_catalogo_padre'=>15000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>15003,
            'descripcion' => 'Granados',
            'id_catalogo_padre'=>15000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>15004,
            'descripcion' => 'Purulhá',
            'id_catalogo_padre'=>15000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>15005,
            'descripcion' => 'Rabinal',
            'id_catalogo_padre'=>15000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>15006,
            'descripcion' => 'Salamá',
            'id_catalogo_padre'=>15000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>15007,
            'descripcion' => 'San Miguel Chicaj',
            'id_catalogo_padre'=>15000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>15008,
            'descripcion' => 'San Jerónimo',
            'id_catalogo_padre'=>15000,
        ]); 
        //Municipios alta verapaz

        DB::table('catalogos')->insert([
            'id_catalogo'=>16001,
            'descripcion' => 'Cobán',
            'id_catalogo_padre'=>16000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>16002,
            'descripcion' => 'San Pedro Carchá',
            'id_catalogo_padre'=>16000,
        ]);
        DB::table('catalogos')->insert([
            'id_catalogo'=>16003,
            'descripcion' => 'San Juan Chamelco',
            'id_catalogo_padre'=>16000,
        ]);
        DB::table('catalogos')->insert([
            'id_catalogo'=>16004,
            'descripcion' => 'San Cristóbal Verapaz',
            'id_catalogo_padre'=>16000,
        ]);
        DB::table('catalogos')->insert([
            'id_catalogo'=>16005,
            'descripcion' => 'Tactic',
            'id_catalogo_padre'=>16000,
        ]);
        DB::table('catalogos')->insert([
            'id_catalogo'=>16006,
            'descripcion' => 'Tucurú',
            'id_catalogo_padre'=>16000,
        ]);
        DB::table('catalogos')->insert([
            'id_catalogo'=>16007,
            'descripcion' => 'Tamahú',
            'id_catalogo_padre'=>16000,
        ]);
        DB::table('catalogos')->insert([
            'id_catalogo'=>16008,
            'descripcion' => 'Panzós',
            'id_catalogo_padre'=>16000,
        ]);
        DB::table('catalogos')->insert([
            'id_catalogo'=>16009,
            'descripcion' => 'Senahú',
            'id_catalogo_padre'=>16000,
        ]);
        DB::table('catalogos')->insert([
            'id_catalogo'=>16010,
            'descripcion' => 'Cahabón',
            'id_catalogo_padre'=>16000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>16011,
            'descripcion' => 'Lanquín',
            'id_catalogo_padre'=>16000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>16012,
            'descripcion' => 'Chahal',
            'id_catalogo_padre'=>16000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>16013,
            'descripcion' => 'Fray Bartolomé de las Casas',
            'id_catalogo_padre'=>16000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>16014,
            'descripcion' => 'Chisec',
            'id_catalogo_padre'=>16000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>16015,
            'descripcion' => 'Santa Cruz Verapaz',
            'id_catalogo_padre'=>16000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>16016,
            'descripcion' => 'Santa Catalina La Tinta',
            'id_catalogo_padre'=>16000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>16017,
            'descripcion' => 'Raxruhá',
            'id_catalogo_padre'=>16000,
        ]); 

        //Municipios de peten
        DB::table('catalogos')->insert([
            'id_catalogo'=>17001,
            'descripcion' => 'Dolores',
            'id_catalogo_padre'=>17000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>17002,
            'descripcion' => 'San Benito',
            'id_catalogo_padre'=>17000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>17003,
            'descripcion' => 'Flores',
            'id_catalogo_padre'=>17000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>17004,
            'descripcion' => 'San Francisco',
            'id_catalogo_padre'=>17000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>17005,
            'descripcion' => 'La Libertad',
            'id_catalogo_padre'=>17000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>17006,
            'descripcion' => 'San José',
            'id_catalogo_padre'=>17000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>17007,
            'descripcion' => 'Melchor de Mencos',
            'id_catalogo_padre'=>17000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>17008,
            'descripcion' => 'San Luis',
            'id_catalogo_padre'=>17000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>17009,
            'descripcion' => 'Poptún',
            'id_catalogo_padre'=>17000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>17010,
            'descripcion' => 'Santa Ana',
            'id_catalogo_padre'=>17000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>17011,
            'descripcion' => 'San Andrés',
            'id_catalogo_padre'=>17000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>17012,
            'descripcion' => 'Sayaxché',
            'id_catalogo_padre'=>17000,
        ]); 
        
        //municipios de izabal
        DB::table('catalogos')->insert([
            'id_catalogo'=>18001,
            'descripcion' => 'Puerto Barrios',
            'id_catalogo_padre'=>18000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>18002,
            'descripcion' => 'Livingston',
            'id_catalogo_padre'=>18000,
        ]); 

        DB::table('catalogos')->insert([
            'id_catalogo'=>18003,
            'descripcion' => 'El Estor',
            'id_catalogo_padre'=>18000,
        ]); 

        DB::table('catalogos')->insert([
            'id_catalogo'=>18004,
            'descripcion' => 'Morales',
            'id_catalogo_padre'=>18000,
        ]); 

        DB::table('catalogos')->insert([
            'id_catalogo'=>18005,
            'descripcion' => 'Los Amates',
            'id_catalogo_padre'=>18000,
        ]); 


        //municipios de zacapa
        DB::table('catalogos')->insert([
            'id_catalogo'=>19001,
            'descripcion' => 'Cabañas',
            'id_catalogo_padre'=>19000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>19002,
            'descripcion' => 'Estanzuela',
            'id_catalogo_padre'=>19000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>19003,
            'descripcion' => 'Gualán',
            'id_catalogo_padre'=>19000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>19004,
            'descripcion' => 'Huité',
            'id_catalogo_padre'=>19000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>19005,
            'descripcion' => 'La Unión',
            'id_catalogo_padre'=>19000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>19006,
            'descripcion' => 'Río Hondo',
            'id_catalogo_padre'=>19000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>19007,
            'descripcion' => 'San Diego',
            'id_catalogo_padre'=>19000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>19008,
            'descripcion' => 'Teculután',
            'id_catalogo_padre'=>19000,
        ]); 

        DB::table('catalogos')->insert([
            'id_catalogo'=>19009,
            'descripcion' => 'Usumatlán',
            'id_catalogo_padre'=>19000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>19010,
            'descripcion' => 'Zacapa',
            'id_catalogo_padre'=>19000,
        ]); 
        
        //Municipios de Chiquimula
        DB::table('catalogos')->insert([
            'id_catalogo'=>20001,
            'descripcion' => 'Chiquimula',
            'id_catalogo_padre'=>20000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>20002,
            'descripcion' => 'Camotán',
            'id_catalogo_padre'=>20000,
        ]);
        DB::table('catalogos')->insert([
            'id_catalogo'=>20003,
            'descripcion' => 'Concepción Las Minas',
            'id_catalogo_padre'=>20000,
        ]);
        DB::table('catalogos')->insert([
            'id_catalogo'=>20004,
            'descripcion' => 'Esquipulas',
            'id_catalogo_padre'=>20000,
        ]);
        DB::table('catalogos')->insert([
            'id_catalogo'=>20005,
            'descripcion' => 'Ipala',
            'id_catalogo_padre'=>20000,
        ]);
        DB::table('catalogos')->insert([
            'id_catalogo'=>20006,
            'descripcion' => 'Jocotán',
            'id_catalogo_padre'=>20000,
        ]);
        DB::table('catalogos')->insert([
            'id_catalogo'=>20007,
            'descripcion' => 'Olopa',
            'id_catalogo_padre'=>20000,
        ]);
        DB::table('catalogos')->insert([
            'id_catalogo'=>20008,
            'descripcion' => 'Quezaltepeque',
            'id_catalogo_padre'=>20000,
        ]);
        DB::table('catalogos')->insert([
            'id_catalogo'=>20009,
            'descripcion' => 'San José La Arada',
            'id_catalogo_padre'=>20000,
        ]);
        DB::table('catalogos')->insert([
            'id_catalogo'=>20010,
            'descripcion' => 'San Juan Ermita',
            'id_catalogo_padre'=>20000,
        ]);
        DB::table('catalogos')->insert([
            'id_catalogo'=>20011,
            'descripcion' => 'San Jacinto',
            'id_catalogo_padre'=>20000,
        ]);

        //Departamentos de Jalapa
        DB::table('catalogos')->insert([
            'id_catalogo'=>21001,
            'descripcion' => 'Jalapa',
            'id_catalogo_padre'=>21000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>21002,
            'descripcion' => 'San Pedro Pinula',
            'id_catalogo_padre'=>21000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>21003,
            'descripcion' => 'San Luis Jilotepeque',
            'id_catalogo_padre'=>21000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>21004,
            'descripcion' => 'San Manuel Chaparrón',
            'id_catalogo_padre'=>21000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>21005,
            'descripcion' => 'San Carlos Alzatate',
            'id_catalogo_padre'=>21000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>21006,
            'descripcion' => 'Monjas',
            'id_catalogo_padre'=>21000,
        ]); 
        DB::table('catalogos')->insert([
            'id_catalogo'=>21007,
            'descripcion' => 'Mataquescuintla',
            'id_catalogo_padre'=>21000,
        ]); 
       

    }
}
